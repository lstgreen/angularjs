/**
 * @ngdoc directive
 * @name decHtml5TableVisibilitySettings
 * @restrict E
 *
 * @description
 *
 * Displays Visibility Settings Modal Window
 *
 * ## Example
 *
 * *HTML*
 * ```html
 *  <dec-html5-table-visibility-settings></dec-html5-table-visibility-settings>
 * ```
 */


(function (angular) {
    'use strict';

    angular.module('dec').directive('decHtml5TableVisibilitySettings', [function () {


        return {
            restrict: 'A'
        };
    }]);

})(angular);
