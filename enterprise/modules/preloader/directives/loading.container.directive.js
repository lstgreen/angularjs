angular.module('bricata.ui.grid')
    .directive('loadingContainer', ["$compile", function($compile) {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var loadingLayer = $compile('<div class="loading">'+
                    '<preloader additional-class="preloader-repositioned"></preloader></div>')(scope);
                element.append(loadingLayer);
                element.addClass('loading-container');
                scope.$watch(attrs.loadingContainer, function(value) {
                    loadingLayer.toggleClass('ng-hide', !value);
                });
            }
        };
    }]);
