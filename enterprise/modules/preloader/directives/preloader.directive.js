angular.module("bricata.ui.preloader")
    .directive("preloader", [
        function () {
            return {
                restrict: 'E',
                templateUrl: 'modules/preloader/views/preloader.html',
                scope: {
                  textBundleKey: "@",
                  additionalClass: "@"
                }
            };
        }]);