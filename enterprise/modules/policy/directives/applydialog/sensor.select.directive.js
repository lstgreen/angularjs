angular.module("bricata.ui.policy")
    .directive("sensorSelect", [
        function () {

            return {
                restrict: 'E',
                templateUrl: 'modules/policy/views/applydialog/sensor-select.html'
            };
        }]);