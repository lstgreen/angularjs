angular.module("bricata.ui.policy")
    .directive("applyPolicy", [
    function () {
        return {
            restrict: 'E',
            templateUrl: 'modules/policy/views/applydialog/apply-policy-modal.html'
        };
    }]);
