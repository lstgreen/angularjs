angular.module("bricata.ui.policy")
    .directive("policyHeaderNavigation", [
    function () {

        return {
            restrict: 'E',
            templateUrl: 'modules/policy/views/policyHeaderNav.html',
            link: function(scope) {
                scope.totalPoliciesFound = 0;

                scope.openApplyPolicyDialog = function() {
                    var eventData = {
                        actionName: "policyApply",
                        actionType: "modal",
                        data: []
                    };

                    scope.$broadcast('grid.header.invoke.row.action', eventData);
                };

                scope.$on('policies.total.rows.change.event', function(event, totalCount) {
                    scope.totalPoliciesFound = totalCount;
                });
            }
        };
    }]);