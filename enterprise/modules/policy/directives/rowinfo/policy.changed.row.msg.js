angular.module("bricata.ui.policy")
    .directive("policyGridRowChangedMsg", [
        function() {
            return {
                restrict : 'E',
                templateUrl : 'modules/policy/views/rowinfo/changed/policy-grid-row-changed-content.html',
                scope: {
                    changedObject: "=",
                    cancelApplication: "&",
                    applyPolicy: "&",
                    cancelDeletion: "&"
                },
                link: function(scope) {
                    //scope.appliedSensorsNumber = 12;

                    scope.applicationCancelHandler = function() {
                        //alert(scope.changedObject.item.name);
                        scope.cancelApplication()();
                    };

                    scope.applyCreatedHandler = function() {
                        scope.applyPolicy()();
                    };

                    scope.deletionCancelHandler = function() {
                        //alert(scope.changedObject.item.name);
                        scope.cancelDeletion()();
                    };
                }
            };
        }]);