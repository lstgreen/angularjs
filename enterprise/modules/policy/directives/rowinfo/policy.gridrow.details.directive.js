angular.module("bricata.ui.policy")
    .directive("policyGridRowDetails", ['PolicyDataService', 'CommonErrorMessageService',
        function(PolicyDataService, CommonErrorMessageService) {
            return {
                restrict : 'E',
                templateUrl : 'modules/policy/views/rowinfo/details/policy-grid-row-details-content.html',
                link: function(scope) {
                    scope.policyDetailsLoaded = false;
                    scope.policyDetailsLoadError = false;

                    scope.loadPolicyDetails = function() {
                        PolicyDataService.getSensors(scope.selectedRow.id).then(function success(data) {
                            scope.sensorsData = data;

                            scope.policyDetailsLoaded = scope.sensorsData && scope.signaturesData;
                        }, function error() {
                            scope.handleError();
                        });

                        PolicyDataService.getSignatures(scope.selectedRow.id).then(function success(data) {
                            scope.signaturesData = data;

                            scope.policyDetailsLoaded = scope.sensorsData && scope.signaturesData;
                        }, function error(reason) {
                            scope.handleError(reason);
                        });
                    };

                    scope.collapseDetails = function() {
                        scope.selectedRow.$selected = false;
                    };

                    scope.handleError = function(reason) {
                        if (!scope.policyDetailsLoadError) {
                            scope.policyDetailsLoadError = true;
                            CommonErrorMessageService.showErrorMessage("errors.policyDetailsDataError", reason, null,
                                scope.processError);
                        }
                    };

                    scope.processError = function() {
                        scope.policyDetailsLoaded = true;
                    };

                    scope.reloadPolicyDetails = function() {
                        scope.policyDetailsLoaded = false;
                        scope.policyDetailsLoadError = false;

                        scope.loadPolicyDetails();
                    };


                    scope.loadPolicyDetails();
                }
            };
        }]);