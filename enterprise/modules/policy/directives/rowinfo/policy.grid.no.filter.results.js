angular.module("bricata.ui.policy")
    .directive("policyGridNoFilterResults", [
        function() {
            return {
                restrict : 'E',
                templateUrl : 'modules/policy/views/rowinfo/nofilterdata/policy-grid-no-filter-results-content.html',
                scope: {
                    clearFilters: "&"
                },
                link: function(scope) {
                    scope.clearFilterCriteria = function() {
                        scope.clearFilters()();
                    };
                }
            };
        }]);