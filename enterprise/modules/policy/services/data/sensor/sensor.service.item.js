angular.module('bricata.ui.policy')
    .factory('SensorItem', ['$resource', 'BricataUris',
        function($resource, BricataUris){
            return $resource(BricataUris.sensorItems, {}, {
                query: {method:'GET', isArray:false}
            });
        }]);