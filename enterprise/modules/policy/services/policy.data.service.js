angular.module("bricata.ui.policy")
    .factory("PolicyDataService", ["$q", "BricataUris", "PolicyItem", "PolicyDetailsInfo", "PolicyItems", "SensorItem",
        function($q, BricataUris, PolicyItem, PolicyDetailsInfo, PolicyItems, SensorItem){
            var policyCloneData;
            var policyEditData;

            var service = {
                getPolicy:function(policyId){
                    var deferred = $q.defer();
                    PolicyItem.query({id: '/'+policyId}).$promise.then(function (receivedData){
                        deferred.resolve(receivedData);
                    }, function() {
                        deferred.reject();
                    });

                    return deferred.promise;
                },
                deletePolicies:function(policyIds){
                    var deferred = $q.defer();
                    var queryObject = {ids: policyIds};
                    PolicyItem.delete({q: JSON.stringify(queryObject)})
                        .$promise.then(function (receivedData){
                            deferred.resolve(receivedData);
                        }, function() {
                            deferred.reject();
                        });

                    return deferred.promise;
                },
                applyPolicy:function(applyData) {
                    var deferred = $q.defer();
                    PolicyItem.create({id: '/' + applyData.policyId + '/apply'}, applyData)
                        .$promise.then(function (receivedStatus){
                            deferred.resolve(receivedStatus);
                        }, function() {
                            deferred.reject();
                        });

                    return deferred.promise;
                },
                createNewPolicy:function(newPolicyObject) {
                    var deferred = $q.defer();
                    PolicyItem.create({id: ''}, newPolicyObject)
                        .$promise.then(function (receivedStatus){
                            deferred.resolve(receivedStatus);
                        }, function() {
                            deferred.reject();
                        });

                    return deferred.promise;
                },
                editPolicy:function(editData) {
                    var deferred = $q.defer();
                    PolicyItem.edit({id: '/' + editData.id}, editData)
                        .$promise.then(function (receivedStatus){
                            deferred.resolve(receivedStatus);
                        }, function() {
                            deferred.reject();
                        });

                    return deferred.promise;
                },
                getSensors:function(queryParam){
                    var deferred = $q.defer();
                    PolicyDetailsInfo.query({entityId: BricataUris.policyDetailSensors, rowId: queryParam})
                        .$promise.then(function (receivedData){
                            deferred.resolve(receivedData.objects);
                        }, function() {
                            deferred.reject();
                        });

                    return deferred.promise;
                },
                getSignatures:function(queryParam){
                    var deferred = $q.defer();
                    PolicyDetailsInfo.query({entityId: BricataUris.policyDetailSignatures, rowId: queryParam})
                        .$promise.then(function (receivedData){
                            deferred.resolve(receivedData.objects);
                        }, function() {
                            deferred.reject();
                        });

                    return deferred.promise;
                },
                getPolicyNames:function(){
                    var deferred = $q.defer();
                    PolicyItems.query({results_per_page: 1000}).$promise.then(function (receivedData){

                        receivedData.objects.sort(function (a, b) {
                            if (a.name > b.name) {
                                return 1;
                            }
                            if (a.name < b.name) {
                                return -1;
                            }
                            return 0;
                        });
                        deferred.resolve(receivedData.objects);
                    }, function(error) {
                        deferred.reject();
                    });

                    return deferred.promise;
                },
                getAllSensors:function(){
                    var deferred = $q.defer();
                    SensorItem.query().$promise.then(function (receivedData){
                        deferred.resolve(receivedData.objects);
                    }, function(error) {
                        deferred.reject();
                    });

                    return deferred.promise;
                },
                storePolicyCloneData:function(cloneData) {
                    policyCloneData = cloneData;
                },
                getPolicyCloneData:function() {
                    return policyCloneData;
                },
                consumePolicyCloneData:function() {
                    policyCloneData = undefined;
                },
                storePolicyEditData:function(editData) {
                    policyEditData = editData;
                },
                getPolicyEditData:function() {
                    return policyEditData;
                },
                consumePolicyEditData:function() {
                    policyEditData = undefined;
                }
            };
            return service;
        }]);