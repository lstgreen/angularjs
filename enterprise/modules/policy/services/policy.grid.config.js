angular.module("bricata.ui.policy")
    .factory("PolicyGridConfigurationService", ['BricataUris', 'CommonNavigationService', 'PolicyDataService',
        function(BricataUris, CommonNavigationService, PolicyDataService){

        var service = {
            getGridConfiguration:function(){

                var config = {};
                config.url = BricataUris.policyItems;

                config.columns = [
                    {'field' : '', 'type': 'check', 'title': '', 'sortable': false, 'style': ''},
                    {'field' : 'name', 'type': 'text', 'title': 'policyGrid.nameCol', 'sortable': true,
                        'style': {'min-width': '220px', 'max-width': '220px'}},
                    {'field' : 'description', 'type': 'text', 'title': 'policyGrid.descCol', 'sortable': true,
                        'style': {'min-width': '290px', 'max-width': '290px'}},
                    {'field' : 'signatures_count', 'type': 'number', 'title': 'policyGrid.signaturesCol',
                        'sortable': false, 'style': {'min-width': '100px', 'max-width': '100px'}},
                    {'field' : 'is_applied', 'type': 'dynamic', 'title': 'policyGrid.appliedCol', 'sortable': true,
                        'style': {'min-width': '100px', 'max-width': '100px'},
                        'values': [
                            {'match': true, 'value': 'policyTypeFilter.appliedTxtValue'},
                            {'match': false, 'value': 'policyTypeFilter.notAppliedTxtValue'}
                        ]
                    },
                    {'field' : 'created_at', 'type': 'date', 'title': 'policyGrid.createdCol',
                        'sortable': true, 'style': {'min-width': '115px', 'max-width': '115px'}},
                    {'field' : 'author', 'subfield': 'name', 'type': 'text', 'title': 'policyGrid.authorCol',
                        'sortable': true, 'style': {'min-width': '140px', 'max-width': '140px'}},
                    {'field' : '', 'type': 'action', 'title': 'policyGrid.actionCol', 'sortable': false,
                        'style': {'min-width': '110px', 'max-width': '110px'},
                        'actions': [
                            {'label': 'policyGrid.actionClone', 'icon': 'glyphicon-duplicate',
                                'actionName': 'policyClone', 'actionType': 'redirect'},
                            {'label': 'policyGrid.actionApply', 'icon': 'glyphicon-ok',
                                'actionName': 'policyApply', 'actionType': 'modal'},
                            {'label': 'policyGrid.actionEdit', 'icon': 'glyphicon-pencil',
                                'actionName': 'policyEdit', 'actionType': 'redirect'},
                            {'label': 'policyGrid.actionDelete', 'icon': 'glyphicon-remove',
                                'actionName': 'policyDelete', 'actionType': 'modal'}
                        ]
                    }
                ];

                config.filters = {
                    searchFilter: {
                        fields: ['name', 'description']
                    },
                    optionFilter: {
                        field: 'is_applied',
                        options: [
                            { value: null, label: 'common.allTxt' },
                            { value: true, label: 'policyTypeFilter.appliedTxt' },
                            { value: false, label: 'policyTypeFilter.notAppliedTxt' }
                        ]
                    },
                    valueFilter: {
                        field: 'author_id',
                        data: {
                            url: BricataUris.policyGridFilterValues,
                            labelField: "name",
                            valueField: "id"
                        }
                    },
                    dateFilter: {
                        field: 'created_at'
                    }
                };

                config.defaultSorting = {field: 'name', direction: 'asc'};
                config.createdSorting = {field: 'created_at', direction: 'desc'};

                config.rowDetailsView = {
                    src: 'modules/policy/views/rowinfo/details/policy-grid-row-details-main.html',
                    stateChangeSrc: 'modules/policy/views/rowinfo/changed/policy-grid-row-changed-main.html',
                    noFilterDataSrc: 'modules/policy/views/rowinfo/nofilterdata/policy-grid-no-filter-results-main.html'
                };

                return config;
            },

            getActionModal:function(actionObject){
                var modalConfiguration = {};
                var processedAction = '';
                switch (actionObject.actionName) {
                    case 'policyApply' :
                        modalConfiguration = {
                            templateUrl: 'modules/policy/views/applydialog/apply-policy-modal.html',
                            controller: 'ApplyPolicyController',
                            windowClass: 'apply-policy-modal-window',
                            resolve: {
                                policyObjects: function(){
                                    return actionObject.data;
                                }
                            }
                        };
                        processedAction = 'change';
                        break;
                    case 'bulkDelete':
                    case 'policyDelete':
                        modalConfiguration = {
                            templateUrl: 'modules/policy/views/delete-policy-modal.html',
                            controller: 'DeletePolicyController',
                            size: 'sm',
                            resolve: {
                                policyObjects: function(){
                                    return actionObject.data;
                                }
                            }
                        };
                        processedAction = 'delete';
                        break;
                }

                return {config: modalConfiguration, action: processedAction};
            },

            performRedirect: function(actionObject) {
                switch (actionObject.actionName) {
                    case 'policyClone' :
                        PolicyDataService.storePolicyCloneData(actionObject.data[0]);
                        break;
                    case 'policyEdit':
                        PolicyDataService.storePolicyEditData(actionObject.data[0]);
                        break;
                }

                CommonNavigationService.navigateTo(BricataUris.policyWizardPage, 'internal');
            }
        };
        return service;
    }]);