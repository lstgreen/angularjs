angular.module('bricata.ui.policy')
    .controller('DeletePolicyController',
    ['$scope', '$modalInstance', 'CommonModalService',
        'policyObjects', 'PolicyDataService', 'CommonErrorMessageService',
        function($scope, $modalInstance, CommonModalService,
                 policyObjects, PolicyDataService, CommonErrorMessageService) {

            $scope.policyItem = policyObjects[0];
            $scope.policyItems = policyObjects;

            $scope.isBulkMode = policyObjects.length > 1;
            $scope.isSendingDataInProgress = false;

            $scope.deleteThisPolicy = function () {
                $scope.isSendingDataInProgress = true;
                var policyIds = [];

                angular.forEach($scope.policyItems, function(item) {
                    policyIds.push(item.id);
                });

                PolicyDataService.deletePolicies(policyIds).then(function() {
                        $modalInstance.close($scope.policyItems);
                        CommonModalService.unbindRepositionOnResize();
                    }, function(reason) {
                        $modalInstance.dismiss('cancel');
                        CommonErrorMessageService.showErrorMessage('errors.deletePolicyError', reason);
                    }
                );
            };

            $scope.closeDeletePolicyModal = function () {
                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();
            };

            $modalInstance.opened.then(function() {
                CommonModalService.centerModal();
                CommonModalService.bindRepositionOnResize();
            });

        }]);
