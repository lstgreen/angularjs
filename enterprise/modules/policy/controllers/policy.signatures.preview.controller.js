angular.module('bricata.ui.policy')
    .controller('PolicySignaturesPreviewController',
    ['$scope', '$modalInstance', 'CommonModalService', '$timeout', 'signatures', 'submitCallback', 'submitBtnLbl',
        function($scope, $modalInstance, CommonModalService, $timeout, signatures, submitCallback, submitBtnLbl) {

            $scope.selectedSignatures = signatures;
            $scope.submitLbl = submitBtnLbl;

            $scope.cancelModal = function () {
                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();
            };

            $scope.submitModal = function () {
                submitCallback();

                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();
            };

            $modalInstance.opened.then(function() {
                CommonModalService.centerModal();
                CommonModalService.bindRepositionOnResize();

                $timeout(function(){
                    $scope.$broadcast('content.changed');
                }, 333, false);
            });

        }]);
