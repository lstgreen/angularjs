angular.module('bricata.ui.policy')
    .controller('CreatePolicyController',
    ['$scope', '$rootScope', '$i18next', 'CommonModalService', 'CommonNavigationService', 'BroadcastService',
        'PolicyDataService', 'ConfigurationService', 'CommonErrorMessageService', 'ValidationService',
        'SignatureSendPreviewGet',
    function($scope, $rootScope, $i18next, CommonModalService, CommonNavigationService, BroadcastService,
             PolicyDataService, ConfigurationService, CommonErrorMessageService, ValidationService,
             SignatureSendPreviewGet) {

        $scope.pageTitleI18N = 'createPolicy.mainTitle';
        $scope.btnTitleI18N = 'createPolicy.createNewPolicy';

        $scope.isEditMode = false;
        $scope.isCloneMode = false;
        $scope.preselectedData = [];

        $scope.isPolicyDataSendingInProgress = false;
        $scope.signatureSelectionEnabled = false;

        $scope.policyData = {
            id: -1,
            policyName: '',
            description: '',
            type: undefined,
            selectedSignatures: []
        };

        $scope.previewBeforeSavingCheckboxModel = {
            checked : false
        };

        $scope.formData = {};

        $scope.policyTypeList = ConfigurationService.getPolicyTypes();
        $scope._policyType = undefined;
        $scope.signatureSelectionMode = '';

        $scope.formData.validation = {
            name: false,
            description: false,
            category: false,
            signatures: false
        };

        $scope.formData.isValid = false;

        $scope.$on('input.text.validation.processed', function(event, data) {
            switch (data.name) {
                case 'policyNameValidation':
                    $scope.formData.validation.name = data.isValid;
                    break;
                case 'policyDescriptionValidation':
                    $scope.formData.validation.description = data.isValid;
                    break;
                case 'policyCategoryValidation':
                    $scope.formData.validation.category = data.isValid;
                    break;
            }

            $scope.checkValidationResult();
        });

        $scope.$on('signature.select.validation.processed', function(event, data) {
            $scope.formData.validation.signatures = data.isValid;

            $scope.checkValidationResult();
        });

        $scope.checkValidationResult = function() {
            if ($scope.signatureSelectionEnabled) {
                $scope.formData.isValid = $scope.formData.validation.name && $scope.formData.validation.description &&
                    $scope.formData.validation.category &&
                    ($scope.formData.validation.signatures || $scope.policyData.selectedSignatures.length > 0);
            } else {
                $scope.formData.isValid = $scope.formData.validation.name && $scope.formData.validation.description &&
                    $scope.formData.validation.category;
            }
        };

        // Submit New Policy Form
        $scope.submitPolicy = function() {
            $scope.checkValidationResult();
            if (!$scope.formData.isValid) {
                $rootScope.$broadcast('run.validation');
                return;
            }

            // If the "Preview Signatures before saving" checkbox is checked
            if ($scope.previewBeforeSavingCheckboxModel.checked === true) {

                var signaturesIDs = [];
                angular.forEach($scope.policyData.selectedSignatures, function(value, key) {
                    signaturesIDs.push(value.id);
                });
                // Sending Signatures IDs to the preview api url
                SignatureSendPreviewGet.get({'q': '{"ids:"' + '[' + signaturesIDs.join(',') + ']}'});

                CommonModalService.show({
                    templateUrl: 'modules/policy/views/previewPolicyModal.html',
                    size: 'lg',
                    controller: 'PolicySignaturesPreviewController',
                    resolve: {
                        submitCallback: function() {
                            return  $scope.createPolicy;
                        },
                        signatures: function() {
                            return $scope.policyData.selectedSignatures;
                        },
                        submitBtnLbl: function() {
                            return $scope.btnTitleI18N;
                        }
                    }
                });
            }
            else {
                // Run Create Policy Function
                $scope.createPolicy();
            }

        };

        $scope.createPolicy = function() {
            $scope.isPolicyDataSendingInProgress = true;

            var policyObject = {
                id: $scope.policyData.id,
                name: $scope.policyData.policyName,
                description: $scope.policyData.description,
                type: $scope.policyData.type.value
            };

            if ($scope.signatureSelectionEnabled) {
                policyObject.signature_ids = [];

                angular.forEach($scope.policyData.selectedSignatures, function(selectedSignature) {
                    policyObject.signature_ids.push(selectedSignature.id);
                });
            }

            ValidationService.ensureUnique(PolicyDataService.getPolicyNames(), policyObject,
                $scope.handlePolicyNameValidationResult);
        };

        $scope.handlePolicyNameValidationResult = function(isUnique, policyObject) {
            if (isUnique) {
                if ($scope.isEditMode) {
                    PolicyDataService.editPolicy(policyObject).then($scope.successHandler, $scope.errorEditHandler);
                } else {
                    PolicyDataService.createNewPolicy(policyObject).then($scope.successHandler,
                        $scope.errorCreateHandler);
                }
            } else {
                $scope.isPolicyDataSendingInProgress = false;
                CommonErrorMessageService.showErrorMessage("validationErrors.policyNameNotUnique", null,
                    "errors.formDataErrorCommonTitle");
            }
        };

        $scope.successHandler = function(createdPolicy) {
            // Redirecting to policies page with specific parameters
            CommonNavigationService.navigateToPoliciesGridPage();
            BroadcastService.changeTopLevelMessage({
                item: createdPolicy,
                id: createdPolicy.id,
                action: $scope.isEditMode ? 'policy-edited' : 'policy-created'
            });
        };

        $scope.errorCreateHandler = function(reason) {
            $scope.isPolicyDataSendingInProgress = false;
            CommonErrorMessageService.showErrorMessage("errors.createPolicyError", reason);
        };

        $scope.errorEditHandler = function(reason) {
            $scope.isPolicyDataSendingInProgress = false;
            CommonErrorMessageService.showErrorMessage("errors.editPolicyError", reason);
        };

        $scope.cancelCreation = function() {
            CommonNavigationService.navigateToPoliciesGridPage();
        };

        $scope.updatePolicyType = function() {
            if ($scope.policyData.selectedSignatures.length > 0) {
                $scope.validatePolicyTypeChange();
            } else {
                $scope.confirmTypeChange();
            }
        };

        $scope.validatePolicyTypeChange = function() {
            CommonModalService.show({
                templateUrl: 'modules/policy/views/policyTypeChangeConfirmation.html',
                controller: 'PolicyTypeChangeConfirmController',
                resolve: {
                    cancelCallback: function() {
                        return $scope.declineTypeChange;
                    },
                    submitCallback: function() {
                        return  $scope.confirmTypeChange;
                    }
                }
            });

        };

        $scope.confirmTypeChange = function(){
            $scope.policyData.selectedSignatures = [];
            $scope.policyData.type = $scope._policyType;
            if (angular.isDefined($scope.policyData.type)) {
                $scope.signatureSelectionMode = $scope.policyData.type.signatureSelect;
                switch ($scope.policyData.type.signatureSelect) {
                    case 'categories':
                    case 'signatures':
                        $scope.signatureSelectionEnabled = true;
                        break;
                    default:
                        $scope.signatureSelectionEnabled = false;
                }
            }
        };

        $scope.declineTypeChange = function(){
            $scope._policyType = $scope.policyData.type;
        };

        $scope.processClonePolicyAction = function() {
            var clonePolicyData = PolicyDataService.getPolicyCloneData();
            if (angular.isDefined(clonePolicyData)) {
                $scope.isCloneMode = true;

                $scope.pageTitleI18N = 'createPolicy.cloneTitle';
                $scope.btnTitleI18N = 'createPolicy.clonePolicyLbl';

                $scope.policyData.policyName = $i18next('createPolicy.clonePolicyPrefixAndName',
                    { postProcess: 'sprintf', sprintf: [clonePolicyData.name] });

                $scope.populateInputsWithData(clonePolicyData);

                PolicyDataService.consumePolicyCloneData();
            }
        };

        $scope.processEditPolicyAction = function() {
            var editPolicyData = PolicyDataService.getPolicyEditData();
            if (angular.isDefined(editPolicyData)) {
                $scope.isEditMode = true;

                $scope.pageTitleI18N = 'createPolicy.editTitle';
                $scope.btnTitleI18N = 'createPolicy.editPolicyLbl';

                $scope.policyData.id = editPolicyData.id;
                $scope.policyData.policyName = editPolicyData.name;

                $scope.populateInputsWithData(editPolicyData);

                PolicyDataService.consumePolicyEditData();
            }
        };

        $scope.populateInputsWithData = function(dataObj) {
            $scope.policyData.description = dataObj.description;

            $scope.formData.validation = {
                name: true,
                description: true,
                category: true,
                signatures: true
            };

            var selectedPolicyType = {};
            angular.forEach($scope.policyTypeList, function(policyType) {
                if (policyType.value === dataObj.policy_type) {
                    selectedPolicyType = policyType;
                    return;
                }
            });

            if (selectedPolicyType.signatureSelect.length > 0) {
                $scope.isPolicyDataSendingInProgress = true;

                PolicyDataService.getSignatures(dataObj.id).then(function success(data) {
                    $scope.preselectedData = data;

                    $scope.finishUpProcessingDetailsForEditOrClone(dataObj, selectedPolicyType);
                }, function error() {
                    $scope.finishUpProcessingDetailsForEditOrClone(dataObj, selectedPolicyType);
                });
            } else {
                $scope.finishUpProcessingDetailsForEditOrClone(dataObj, selectedPolicyType);
            }
        };

        $scope.finishUpProcessingDetailsForEditOrClone = function(dataObj, selectedPolicyType) {
            $scope.policyData.type = selectedPolicyType;
            $scope._policyType = selectedPolicyType;
            $scope.updatePolicyType();

            $scope.isPolicyDataSendingInProgress = false;
            $scope.checkValidationResult();
        };

        $scope.processClonePolicyAction();
        $scope.processEditPolicyAction();

    }]);
