angular.module('bricata.ui.policy')
    .controller('PolicyTypeChangeConfirmController',
    ['$scope', '$modalInstance', 'CommonModalService', 'cancelCallback', 'submitCallback',
        function($scope, $modalInstance, CommonModalService, cancelCallback, submitCallback) {

            $scope.cancelModal = function () {
                cancelCallback();

                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();
            };

            $scope.submitModal = function () {
                submitCallback();

                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();
            };

            $modalInstance.opened.then(function() {
                CommonModalService.centerModal();
                CommonModalService.bindRepositionOnResize();
            });

        }]);
