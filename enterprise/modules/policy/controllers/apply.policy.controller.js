angular.module('bricata.ui.policy')
    .controller('ApplyPolicyController',
    ['$scope', '$modalInstance', '$rootScope', 'CommonModalService', 'PolicyDataService',
        'ConfigurationService', 'CommonErrorMessageService', 'policyObjects',
    function($scope, $modalInstance, $rootScope, CommonModalService, PolicyDataService,
             ConfigurationService, CommonErrorMessageService, policyObjects) {

        $scope.isDataLoading = true;
        $scope.sensorsLoaded = false;
        $scope.policiesLoaded = false;

        $scope.duplicateSensorFound = false;
        $scope.isApplyPolicyFormInvalid = true;

        $scope.isSensorSelectionEnabled = false;

        $scope.model = {};
        $scope.model.validation = {
            name: policyObjects.length > 0,
            mode: false,
            action: false,
            sensors: false
        };
        $scope.model.isValid = false;

        $scope.model.data = {
            policyId: angular.isDefined(policyObjects[0]) ? policyObjects[0].id : -1,
            isGlobal: true,
            deploymentMode: null,
            actions: [],
            interfaceIDs: []
        };

        $scope.model._policyId = {id: $scope.model.data.policyId};

        $scope.model.values = {
            policies: [],
            sensors: [],
            deployments: ConfigurationService.getPolicyDeploymentModes(),
            actions: ConfigurationService.getPolicyActions()
        };

        $scope.nameInterfaceRowsModel = [{sensor: {}, interface: {}}];

        PolicyDataService.getPolicyNames().then(function success(data) {
            $scope.model.values.policies = data;
            $scope.policiesLoaded = true;
            $scope.isDataLoading = !$scope.sensorsLoaded || !$scope.policiesLoaded;

            CommonModalService.centerModal();
        });

        PolicyDataService.getAllSensors().then(function success(data) {
            $scope.model.values.sensors = data;
            $scope.sensorsLoaded = true;
            $scope.isDataLoading = !$scope.sensorsLoaded || !$scope.policiesLoaded;

            CommonModalService.centerModal();
        });

        $scope.changeHierarchy = function() {
            $scope.isSensorSelectionEnabled = !$scope.model.data.isGlobal;
            $scope.checkValidationResult();
        };

        var unbindInputValidationListener = $scope.$on('input.text.validation.processed', function(event, data) {
            switch (data.name) {
                case 'applyPolicyNameValidation':
                    $scope.model.validation.name = data.isValid;
                    break;
                case 'applyPolicyDeploymentModeValidation':
                    $scope.model.validation.mode = data.isValid;
                    break;
            }

            $scope.checkValidationResult();
        });

        var unbindCheckboxesValidationListener = $scope.$on('checkboxes.validation.processed', function(event, data) {
            switch (data.name) {
                case 'applyPolicyActionValidation':
                    $scope.model.validation.action = data.isValid;
                    break;
            }

            $scope.checkValidationResult();
        });

        var unbindSensorSelectValidationListener = $scope.$on('sensor.select.validation.processed',
            function(event, data) {
                $scope.model.validation.sensors = data.isValid;

                $scope.checkValidationResult();
            });

        $scope.checkValidationResult = function() {
            if ($scope.isSensorSelectionEnabled) {
                $scope.model.isValid = $scope.model.validation.name && $scope.model.validation.mode &&
                    $scope.model.validation.sensors && $scope.model.validation.action;
            } else {
                $scope.model.isValid = $scope.model.validation.name && $scope.model.validation.mode &&
                    $scope.model.validation.action;
            }

            //angular.element(document.querySelector('#applyBtn')).attr('disabled', !$scope.formData.isValid);
        };

        $scope.toggleAction = function toggleSelection(actionName) {
            var idx = $scope.model.data.actions.indexOf(actionName);

            if (idx > -1) {
                $scope.model.data.actions.splice(idx, 1);
            } else {
                $scope.model.data.actions.push(actionName);
            }
        };

        $scope.applyThisPolicy = function () {
            $scope.checkValidationResult();

            if (!$scope.model.isValid) {
                $rootScope.$broadcast('run.validation');
                return;
            }

            $scope.isDataLoading = true;

            $scope.model.data.interfaceIDs = [];
            angular.forEach($scope.nameInterfaceRowsModel, function(interfaceRow) {
                $scope.model.data.interfaceIDs.push(interfaceRow.interface.id);
            });

            PolicyDataService.applyPolicy($scope.model.data).then(function success(data) {
                $modalInstance.close({id: $scope.model.data.policyId, field: 'is_applied',
                    value: 'policyTypeFilter.appliedTxtValue' });
                CommonModalService.unbindRepositionOnResize();

                $scope.$destroy();
            }, function error(reason) {
                $scope.isDataLoading = false;
                CommonErrorMessageService.showErrorMessage("errors.applyPolicyError", reason);
            });
        };

        $scope.closeApplyPolicyModal = function () {
            $modalInstance.dismiss('cancel');
            CommonModalService.unbindRepositionOnResize();

            $scope.$destroy();
        };

        $modalInstance.opened.then(function() {
            CommonModalService.centerModal();
            CommonModalService.bindRepositionOnResize();
        });


        // Add new Interface row
        $scope.addNameInterfaceRow = function () {
            $scope.nameInterfaceRowsModel.push({sensor: {}, interface: {}});
            $scope.$broadcast('content.changed');
            $scope.$emit('scrollable.scroll.bottom');
        };


        // Remove an Interface row
        $scope.removeNameInterfaceRow = function (index) {
            $scope.nameInterfaceRowsModel.splice(index, 1);
            $scope.$broadcast('content.changed');
            $scope.$emit('scrollable.scroll.bottom');
        };

        //cleaning resources
        var unbindDestroy = $scope.$on("$destroy", function() {
            unbindInputValidationListener();
            unbindCheckboxesValidationListener();
            unbindSensorSelectValidationListener();
            unbindDestroy();
        });

    }]);
