angular.module('bricata.ui.validation')
    .factory("ValidationService", ["$timeout",
        function ($timeout) {
            return {
                showErrorHint: function(attributes, elementWithClass, elementWithTooltip, tooltipTxt) {
                    if (attributes.tooltip !== tooltipTxt) {
                        attributes.$set('tooltip', tooltipTxt);
                        if (elementWithClass) {
                            elementWithClass.addClass('has-error');
                        }
                        elementWithTooltip.triggerHandler('validationfailed');
                    }
                },

                hideErrorHint: function(attributes, elementWithClass, elementWithTooltip) {
                    if (attributes.tooltip !== '') {
                        elementWithTooltip.triggerHandler('validationpassed');
                        attributes.$set('tooltip', '');
                        if (elementWithClass) {
                            elementWithClass.removeClass('has-error');
                        }
                    }
                },

                validateLater: function(checkTimer, validationCallback) {
                    if (checkTimer) {
                        $timeout.cancel(checkTimer);
                    }
                    return $timeout(function(){
                        validationCallback();
                    }, 333, false);
                },

                pauseValidation: function(element) {
                    $timeout(function(){
                        element.triggerHandler('validationpassed');
                    }, 1, false);
                },

                resumeValidation: function(element) {
                    $timeout(function(){
                        element.triggerHandler('validationfailed');
                    }, 333, false);
                },

                ensureUnique: function(existingNamesQueryPromise, entity, resultCallback) {
                    var isUnique = true;
                    existingNamesQueryPromise.then(function(policies) {
                        angular.forEach(policies, function(policy) {
                            if (policy.name === entity.name && policy.id !== entity.id) {
                                isUnique = false;
                                return;
                            }
                        });

                        resultCallback(isUnique, entity);
                    });
                }
            };

        }]);