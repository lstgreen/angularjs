angular.module("bricata.ui.validation")
    .directive("signatureSelectValidation", ["$rootScope", "$i18next", "ValidationService",
        function($rootScope, $i18next, ValidationService) {
        return {
            restrict: "A",
            link: function(scope, element, attr) {

                scope.listenersAttached = false;

                $rootScope.$on('run.validation', function(event, data) {
                    if (data && data.group) {
                        return;
                    }
                    scope.addValidationListeners();
                });

                element.bind('mouseover', function() {
                    scope.addValidationListeners();
                });

                scope.addValidationListeners = function() {
                    if (!scope.listenersAttached) {
                        scope.listenersAttached = true;
                        element.off('mouseover');

                        scope.$watch('selectionModel.length', scope.scheduleValidation);
                        scope.scheduleValidation();
                    }
                };

                scope.checkTimer = null;
                scope.scheduleValidation = function() {
                    scope.checkTimer = ValidationService.validateLater(scope.checkTimer, scope.performValidation);
                };

                $rootScope.$on('disable.validation', function() {
                    ValidationService.pauseValidation(element);
                });

                $rootScope.$on('enable.validation', function() {
                    ValidationService.resumeValidation(element);
                });

                scope.performValidation = function() {
                    var validationResult = false;

                    if (scope.selectionModel.length === 0) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next('validationErrors.noSignaturesSelected'));
                    } else {
                        ValidationService.hideErrorHint(attr, element.parent(), element);

                        validationResult = true;
                    }

                    scope.$emit('signature.select.validation.processed',
                        {isValid: validationResult});
                };
            }
        };
    }]);
