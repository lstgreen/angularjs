angular.module("bricata.ui.validation")
    .directive("inputValidation", ["$i18next", "$rootScope", "ValidationService",
        function($i18next, $rootScope, ValidationService) {
        return {
            restrict: "A",
            scope: {
              validationEventName: '@',
              validationResult: '=',
              validationGroup: '@'
            },
            link: function(scope, element, attr) {

                scope.listenersAttached = false;

                $rootScope.$on('run.validation', function(event, data) {
                    if (data && data.group) {
                        if (angular.isDefined(scope.validationGroup) && scope.validationGroup === data.group) {
                            scope.addValidationListeners();
                        }
                    } else {
                        scope.addValidationListeners();
                    }
                });

                element.bind('blur', function() {
                    scope.addValidationListeners();
                });

                var unbindValueWatch = scope.$watch(function() { return element.val(); }, function() {
                    if (element.val().length > 0 && element.val() !== '?') {
                        scope.addValidationListeners();
                    }
                });

                var unbindDisabledWatch = scope.$watch(function() { return element.prop('disabled'); }, function() {
                    if (element.prop('disabled') === true) {
                        scope.addValidationListeners();
                    }
                });

                scope.addValidationListeners = function() {
                    if (!scope.listenersAttached) {
                        element.off('blur');
                        unbindValueWatch();
                        unbindDisabledWatch();
                        scope.listenersAttached = true;

                        scope.$watch(function() { return element.val(); }, scope.scheduleValidation);

                        scope.$watch(function() { return element.prop('disabled'); }, scope.scheduleValidation);

                        scope.scheduleValidation();
                    }
                };

                scope.checkTimer = null;
                scope.scheduleValidation = function() {
                    scope.checkTimer = ValidationService.validateLater(scope.checkTimer, scope.performValidation);
                };

                scope.setTooltip = '';
                $rootScope.$on('disable.validation', function() {
                    ValidationService.pauseValidation(element);
                });

                $rootScope.$on('enable.validation', function() {
                    ValidationService.resumeValidation(element);
                });

                scope.performValidation = function() {
                    var result = false;

                    if (attr.required && (element.val().length === 0 || element.val() === '?')) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next('validationErrors.fieldRequired'));
                    } else if (attr.ngMinlength > 0 && element.val().length < attr.ngMinlength) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next('validationErrors.tooSmall',
                                { postProcess: 'sprintf', sprintf: [attr.ngMinlength] }));
                    } else if (!element.prop('disabled') && attr.ngPattern && !attr.ngPattern.test(element.val())) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next('validationErrors.incorrectValue'));
                    } else {
                        ValidationService.hideErrorHint(attr, element.parent(), element);

                        result = true;
                    }

                    if (angular.isDefined(scope.validationResult)) {
                        scope.validationResult = result;
                    }

                    if (angular.isDefined(scope.validationEventName)) {
                        scope.$emit('input.text.validation.processed',
                            {name: scope.validationEventName, isValid: result});
                    } else {
                        scope.$emit('input.text.validation.processed');
                    }

                };
            }
        };
    }]);
