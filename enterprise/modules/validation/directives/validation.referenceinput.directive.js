angular.module("bricata.ui.validation")
    .directive("referenceInputValidation", ["$i18next", "ValidationService", function($i18next, ValidationService) {
        return {
            restrict: "A",
            scope: {
                rowsModel: "=",
                noDuplicatedReferenceFlag: "="
            },
            link: function(scope, element, attr) {


                element.bind('mouseover', function() {
                    element.off('mouseover');
                    element.bind('click', function() {
                        scope.performValidation();
                    });
                    element.bind('keyup', function() {
                        scope.performValidation();
                    });


                    scope.performValidation();
                    scope.valueChangeListenerAttached = true;
                });

                scope.performValidation = function() {
                    scope.referenceValidationResult = false;

                    var emptyReferenceError = scope.searchForEmptyReference();
                    if (emptyReferenceError.length > 0) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next(emptyReferenceError));

                        scope.$emit('reference.input.validation.processed', {isValid: false});

                        return;
                    }

                    var duplicatedReferenceError = scope.searchForDuplicatedReference();
                    if (duplicatedReferenceError.length > 0) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next(duplicatedReferenceError));

                        scope.$emit('reference.input.validation.processed', {isValid: false});

                        return;
                    }

                    ValidationService.hideErrorHint(attr, element.parent(), element);

                    scope.$emit('reference.input.validation.processed', {isValid: true});
                };

                scope.searchForEmptyReference = function() {
                    var errorMsg = "";
                    angular.forEach(scope.rowsModel, function(entity) {
                        if (entity.typeId && entity.value === '') {
                            errorMsg = "validationErrors.emptyReferenceValue";
                            return;
                        }
                    });

                    return errorMsg;
                };

                scope.searchForDuplicatedReference = function() {
                    var errorMsg = "";
                    scope.noDuplicatedReferenceFlag = true;
                    angular.forEach(scope.rowsModel, function(entity, i) {
                        angular.forEach(scope.rowsModel, function(potentialDuplicate, k) {
                            if (i !== k && entity.typeId && potentialDuplicate.typeId &&
                                entity.typeId === potentialDuplicate.typeId &&
                                entity.value === potentialDuplicate.value) {
                                errorMsg = "validationErrors.duplicatedReference";
                                scope.noDuplicatedReferenceFlag = false;
                                return;
                            }
                        });

                        if (errorMsg.length > 0) {
                            return;
                        }
                    });


                    return errorMsg;
                };
            }
        };
    }]);
