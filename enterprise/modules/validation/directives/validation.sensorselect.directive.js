angular.module("bricata.ui.validation")
    .directive("sensorSelectValidation", ["$i18next", "$rootScope", "ValidationService",
        function($i18next, $rootScope, ValidationService) {
        return {
            restrict: "A",
            scope: {
                rowsModel: "=",
                duplicatedSensorFlag: "="
            },
            link: function(scope, element, attr) {

                scope.listenersAttached = false;

                $rootScope.$on('run.validation', function() {
                    scope.addValidationListeners();
                });

                element.bind('mouseover', function() {
                    scope.addValidationListeners();
                });

                scope.addValidationListeners = function() {
                    if (!scope.listenersAttached) {
                        scope.listenersAttached = true;
                        element.off('mouseover');
                        element.bind('click', function() {
                            scope.scheduleValidation();
                        });

                        scope.scheduleValidation();
                    }
                };

                scope.checkTimer = null;
                scope.scheduleValidation = function() {
                    scope.checkTimer = ValidationService.validateLater(scope.checkTimer, scope.performValidation);
                };

                scope.performValidation = function() {
                    var emptySensorError = scope.searchForEmptySensors();
                    if (emptySensorError.length > 0) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next(emptySensorError));

                        scope.$emit('sensor.select.validation.processed',
                            {isValid: false});

                        return;
                    }

                    var duplicatedSensorError = scope.searchForDuplicatedSensors();
                    if (duplicatedSensorError.length > 0) {
                        ValidationService.showErrorHint(attr, element.parent(), element,
                            $i18next(duplicatedSensorError));

                        scope.$emit('sensor.select.validation.processed',
                            {isValid: false});

                        return;
                    }

                    ValidationService.hideErrorHint(attr, element.parent(), element);

                    scope.$emit('sensor.select.validation.processed',
                        {isValid: true});
                };

               scope.searchForEmptySensors = function() {
                   var errorMsg = "";
                   angular.forEach(scope.rowsModel, function(entity) {
                        if (!angular.isDefined(entity.sensor.name) || entity.sensor.name === '') {
                            errorMsg = "validationErrors.errorEmptyCompositeInput";
                            return;
                        }
                   });

                   return errorMsg;
                };

               scope.searchForDuplicatedSensors = function() {
                   var errorMsg = "";
                   scope.duplicatedSensorFlag = false;
                   angular.forEach(scope.rowsModel, function(entity, i) {
                        angular.forEach(scope.rowsModel, function(potentialDuplicate, k) {
                            if (i !== k && entity.sensor.name === potentialDuplicate.sensor.name &&
                                entity.interface.name === potentialDuplicate.interface.name) {
                                errorMsg = "applyPolicyModal.errorDuplicatedSensors";
                                scope.duplicatedSensorFlag = true;
                                return;
                            }
                        });

                        if (errorMsg.length > 0) {
                            return;
                        }
                   });


                   return errorMsg;
               };
            }
        };
    }]);
