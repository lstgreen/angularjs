angular.module('bricata.ui.grid',
    ['jm.i18next', 'angularMoment', 'ui.bootstrap', 'ngTable', 'ngResource', 'bricata.ui.ipinput',
        'bricata.ui.navigation', 'bricata.ui.api', 'bricata.ui.modal', 'bricata.ui.header',
        'bricata.ui.labletrunc']);