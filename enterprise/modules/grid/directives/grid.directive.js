angular.module("bricata.ui.grid")
    .directive("commonGrid", ['$i18next', 'GridCommonService', 'ngTableParams', 'ConfigurationService',
        'GridConfigurationService',
        function($i18next, GridCommonService, ngTableParams, ConfigurationService, GridConfigurationService) {
        return {
            restrict : 'EA',
            templateUrl : 'modules/grid/views/common-grid.html',
            controller: 'GridController',
            scope: {
                gridConfiguration: "=",
                changedObject: "=",
                rowActionHandler: "&"
            },
            link: function(scope) {
                scope.data = GridCommonService.data;
                scope.columnDefinitions = null;
                scope.gridOptions = {columnDefs: scope.columnDefinitions};
                scope.rowChangedObject = null;

                scope.isGridInited = false;
                scope.dateFormat = $i18next('formats.gridDateFormat');

                scope.selectAllProp = undefined;

                scope.bulkDeleteDisabled = true;

                var unbindConfigWatcher = scope.$watch('gridConfiguration', function() {
                    if (angular.isDefined(scope.gridConfiguration.columns)) {
                        scope.columnDefinitions = scope.gridConfiguration.columns;

                        scope.initializeGrid();

                        scope.isGridInited = true;

                        unbindConfigWatcher();
                    }
                });

                scope.$watch('changedObject', function() {
                    if (scope.changedObject && scope.changedObject.action) {
                        switch (scope.changedObject.action) {
                            case 'change':
                                scope.markChangedRow();
                                break;
                            case 'edit':
                                scope.handleEditStatus();
                                break;
                            case 'delete':
                                scope.handleDeleteStatus();
                                break;
                            case 'create':
                                scope.handleCreateStatus();
                                break;
                        }
                    }
                });

                scope.initializeGrid = function() {
                    var savedSorting = GridConfigurationService.getSorting();
                    scope.sorting = angular.copy(savedSorting ? savedSorting : scope.gridConfiguration.defaultSorting);

                    scope.pageSize = {};
                    scope.pageSize.options = ConfigurationService.getGridPageSizes();
                    var savedPageSize = GridConfigurationService.getPageSize();
                    scope.pageSize.selection = savedPageSize > -1 ? savedPageSize : scope.pageSize.options[0];

                    scope.pagination = {};
                    var savedPageNumber = GridConfigurationService.getPageNumber();
                    scope.pagination.currentPage = savedPageNumber > -1 ? savedPageNumber : 1;
                    scope.pagination.maxSize = 5;
                    scope.pagination.totalItemsCount = 0;
                    scope.pagination.loadedItemsCount = 0;

                    var noneRestorePending = GridConfigurationService.isFilterStateEmpty();
                    scope.initializeData = function() {
                        var initialSorting = {};
                        initialSorting[scope.sorting.field] = scope.sorting.direction;
                        scope.tableParams = new ngTableParams(
                            {
                                page: scope.pagination.currentPage,
                                sorting: initialSorting
                            },
                            {
                                total: 0, // length of data
                                getData: function($defer, params) {

                                    GridCommonService.getData($defer,params,'',
                                        {
                                            entityId: scope.gridConfiguration.url,
                                            query: scope.prepareQueryData(),
                                            page: scope.pagination.currentPage,
                                            pagesize: scope.pageSize.selection
                                        },
                                        scope.dateFormat, scope.gridConfiguration.columns,
                                        scope.handleNewDataLoaded, scope.handleErrorData,
                                        noneRestorePending);

                                    noneRestorePending = true;
                                }
                            });
                    };

                    scope.initializeData();
                    scope.enableCheckBoxes();
                };

                scope.enableCheckBoxes = function() {
                    scope.checkboxes = { 'checked': false, items: {} };

                    scope.selectAll = function($event) {
                        var value = $event.target.checked;
                        scope.checkboxes.checked = value;
                        angular.forEach(scope.tableParams.data, function(item) {
                            if (angular.isDefined(item.id)) {
                                scope.checkboxes.items[item.id] = value;
                            }
                        });

                        scope.processCheckBoxesChange();
                    };

                    scope.processCheckBoxesChange = function() {
                        if (!scope.tableParams.data || scope.tableParams.data.length === 0) {
                            return;
                        }

                        var checked = 0, unchecked = 0,
                            total = scope.tableParams.data.length;

                        angular.forEach(scope.tableParams.data, function(item) {
                            checked   +=  (scope.checkboxes.items[item.id]) || 0;
                            unchecked += (!scope.checkboxes.items[item.id]) || 0;
                        });

                        if ((unchecked === 0) || (checked === 0)) {
                            scope.checkboxes.checked = (checked === total);
                        }

                        // grayed checkbox
                        scope.selectAllProp = checked !== 0 && unchecked !== 0;

                        scope.bulkDeleteDisabled = checked === 0;
                    };
                };
            }
        };
    }]);