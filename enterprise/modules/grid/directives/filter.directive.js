angular.module("bricata.ui.grid")
    .directive("commonFilter", ["$i18next", "$timeout", "moment", "FilterValueItem", "GridConfigurationService",
        function($i18next, $timeout, moment, FilterValueItem, GridConfigurationService) {
            return {
                restrict : 'EA',
                templateUrl : 'modules/grid/views/common-filter.html',
                scope: {
                    filterConfig: "=",
                    filterChanged: "&"
                },
                link: function(scope) {
                    scope.selectedFilters = {};

                    scope.clearFilterState = 'disabled';

                    scope.isSearchFilterInited = false;
                    scope.isValueFilterInited = false;
                    scope.isOptionFilterReady = false;

                    scope.isDateFilterInited = false;

                    scope.datePickerOptions = {
                        startingDay: $i18next('formats.startingDay')
                    };

                    var unbindFilterConfigWatcher = scope.$watch('filterConfig', function(value) {
                        if (value.valueFilter) {
                            scope.loadFilterValueDataAndContinueInit();
                        } else {
                            scope.initializeOtherFilters();
                        }

                        unbindFilterConfigWatcher();

                    });

                    scope.initializeOtherFilters = function() {
                        if (scope.filterConfig.searchFilter) {
                            scope.initializeSearchFilter();
                        }

                        if (scope.filterConfig.optionFilter) {
                            scope.initializeOptionFilter();
                        }

                        if (scope.filterConfig.dateFilter) {
                            scope.initializeDateFilter();
                        }

                        if (!GridConfigurationService.isFilterStateEmpty()) {
                            scope.handleFilterChange();
                        }
                    };

                    scope.loadFilterValueDataAndContinueInit = function() {
                        FilterValueItem.query({entityId: scope.filterConfig.valueFilter.data.url}).$promise.then(
                            function (receivedData){
                                var allValueItems = receivedData.objects;
                                var valueFilterItems = [];
                                angular.forEach(allValueItems, function(valueItem){
                                    valueFilterItems.push({
                                        value: valueItem[scope.filterConfig.valueFilter.data.valueField],
                                        label: valueItem[scope.filterConfig.valueFilter.data.labelField]
                                    });
                                });

                                scope.initializeValueFilter(valueFilterItems);
                                scope.initializeOtherFilters();
                            }, function() {
                                //alert(error);
                            });
                    };

                    scope.initializeSearchFilter = function() {
                        var savedFilterValue = GridConfigurationService.getFilterState('searchFilterVal');
                        scope.selectedFilters.searchFilterVal = savedFilterValue ? savedFilterValue : '';

                        scope.$watch('selectedFilters.searchFilterVal', function() {
                            scope.handleFilterChange();
                        });

                        scope.isSearchFilterInited = true;
                    };

                    scope.initializeOptionFilter = function() {
                        scope.optionFilterValues = scope.filterConfig.optionFilter.options;

                        var savedFilterValue = GridConfigurationService.getFilterState('optionFilterVal');
                        if (savedFilterValue) {
                            angular.forEach(scope.optionFilterValues, function(optionFilter){
                                if (optionFilter.value === savedFilterValue.value) {
                                    scope.selectedFilters.optionFilterVal = optionFilter;
                                    return;
                                }
                            });
                        } else {
                            scope.selectedFilters.optionFilterVal = scope.optionFilterValues[0];
                        }

                        scope.isOptionFilterReady = true;
                    };

                    scope.initializeValueFilter = function(valueFilterItems) {
                        var defaultFilterValueOption = {
                            value: 'all', label: 'common.everyoneTxt'
                        };

                        valueFilterItems.splice(0, 0, defaultFilterValueOption);

                        scope.valueFilterValues = valueFilterItems;

                        var savedFilterValue = GridConfigurationService.getFilterState('valueFilterVal');
                        if (savedFilterValue) {
                            angular.forEach(scope.valueFilterValues, function(valueFilter){
                                if (valueFilter.value === savedFilterValue.value) {
                                    scope.selectedFilters.valueFilterVal = valueFilter;
                                    return;
                                }
                            });
                        } else {
                            scope.selectedFilters.valueFilterVal = scope.valueFilterValues[0];
                        }

                        scope.isValueFilterInited = true;
                    };

                    scope.initializeDateFilter = function() {
                        scope.selectedFilters.dateStart = null;
                        scope.selectedFilters.dateEnd = null;
                        scope.minDateEnd = null;
                        scope.maxDateStart = null;

                        var savedStartFilter = GridConfigurationService.getFilterState('dateStart');
                        if (savedStartFilter) {
                            scope.selectedFilters.dateStart =
                                moment(savedStartFilter).format($i18next('formats.gridDateFormat'));
                            scope.minDateEnd = savedStartFilter;
                        }

                        var savedEndFilter = GridConfigurationService.getFilterState('dateEnd');
                        if (savedEndFilter) {
                            scope.selectedFilters.dateEnd =
                                moment(savedEndFilter).format($i18next('formats.gridDateFormat'));
                            scope.maxDateStart = savedEndFilter;
                        }

                        scope.startDateChange = function(timeParam) {
                            scope.minDateEnd = timeParam;

                            scope.handleFilterChange();
                        };

                        scope.endDateChange = function(timeParam) {
                            scope.maxDateStart = timeParam;

                            scope.handleFilterChange();
                        };

                        scope.isDateFilterInited = true;
                    };

                    scope.checkTimer = null;
                    scope.handleFilterChange = function() {
                        if (scope.checkTimer) {
                            $timeout.cancel(scope.checkTimer);
                        }

                        scope.checkTimer = $timeout(function(){
                            scope.processFilterChange();
                        }, 111, false);
                    };

                    scope.processFilterChange = function() {
                        var isActive = false;
                        var filterData = [];

                        if (scope.selectedFilters.searchFilterVal) {

                            GridConfigurationService.saveFilterState('searchFilterVal',
                                scope.selectedFilters.searchFilterVal);

                            isActive = true;

                            var searchFilterData = [];
                            angular.forEach(scope.filterConfig.searchFilter.fields, function(field){
                                searchFilterData.push({
                                    "name": field,
                                    "op": "like",
                                    "val": '%' + scope.selectedFilters.searchFilterVal + '%'
                                });
                            });

                            filterData.push({"or": searchFilterData});
                        } else {
                            GridConfigurationService.saveFilterState('searchFilterVal', null);
                        }

                        if (scope.selectedFilters.optionFilterVal &&
                            scope.selectedFilters.optionFilterVal !== scope.optionFilterValues[0]) {

                            GridConfigurationService.saveFilterState('optionFilterVal',
                                scope.selectedFilters.optionFilterVal);

                            isActive = true;

                            filterData.push({
                                "name": scope.filterConfig.optionFilter.field,
                                "op": "==",
                                "val": scope.selectedFilters.optionFilterVal.value
                            });
                        } else {
                            GridConfigurationService.saveFilterState('optionFilterVal', null);
                        }

                        if (scope.selectedFilters.valueFilterVal &&
                            scope.selectedFilters.valueFilterVal !== scope.valueFilterValues[0]) {

                            GridConfigurationService.saveFilterState('valueFilterVal',
                                scope.selectedFilters.valueFilterVal);

                            isActive = true;

                            filterData.push({
                                "name": scope.filterConfig.valueFilter.field,
                                "op": "==",
                                "val": scope.selectedFilters.valueFilterVal.value
                            });
                        } else {
                            GridConfigurationService.saveFilterState('valueFilterVal', null);
                        }

                        if (scope.selectedFilters.dateStart) {
                            isActive = true;

                            filterData.push({
                                "name": scope.filterConfig.dateFilter.field,
                                "op": "ge",
                                "val": scope.selectedFilters.dateStart
                            });

                            GridConfigurationService.saveFilterState('dateStart', scope.selectedFilters.dateStart);
                        } else {
                            GridConfigurationService.saveFilterState('dateStart', null);
                        }

                        if (scope.selectedFilters.dateEnd) {
                            isActive = true;

                            filterData.push({
                                "name": scope.filterConfig.dateFilter.field,
                                "op": "le",
                                "val": scope.selectedFilters.dateEnd
                            });

                            GridConfigurationService.saveFilterState('dateEnd', scope.selectedFilters.dateEnd);
                        } else {
                            GridConfigurationService.saveFilterState('dateEnd', null);
                        }

                        scope.filterChanged()(filterData);

                        scope.clearFilterState = isActive ? 'active' : 'disabled';
                    };

                    scope.clearFilter = function() {
                        GridConfigurationService.clearFilterState();

                        if (scope.selectedFilters.searchFilterVal) {
                            scope.selectedFilters.searchFilterVal = undefined;
                        }
                        if (scope.selectedFilters.optionFilterVal) {
                            scope.selectedFilters.optionFilterVal = scope.optionFilterValues[0];
                        }
                        if (scope.selectedFilters.valueFilterVal) {
                            scope.selectedFilters.valueFilterVal = scope.valueFilterValues[0];
                        }
                        if (scope.selectedFilters.dateStart) {
                            scope.selectedFilters.dateStart = null;
                        }
                        if (scope.selectedFilters.dateEnd) {
                            scope.selectedFilters.dateEnd = null;
                        }
                        scope.handleFilterChange();
                    };

                    scope.$on('clearFilterEvent', function() {
                        scope.clearFilter();
                    });
                }
            };
        }]);