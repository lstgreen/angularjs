angular.module('bricata.ui.grid')
    .controller('GridCommonControl',
    ['$scope', 'GridConfigurationService', 'CommonModalService', 'BroadcastService',
    function($scope, GridConfigurationService, CommonModalService, BroadcastService) {
        $scope.init = function(reportId)
        {
            $scope.reportId = reportId;
            $scope.configuration = GridConfigurationService.getConfiguration($scope.reportId);
            $scope.changedObject = {};
        };

        $scope.lastGridAction = '';

        $scope.gridRowAction = function(actionObject) {
            $scope.changedObject = {};
            switch (actionObject.actionType) {
                case 'modal':
                    var modalData = GridConfigurationService.getModal($scope.reportId, actionObject);
                    $scope.lastGridAction = modalData.action;
                    CommonModalService.show(modalData.config).then(function (modalData) {
                        $scope.changedObject = {data: modalData, action: $scope.lastGridAction};
                    });
                    break;
                case 'redirect':
                    GridConfigurationService.redirectToPage($scope.reportId, actionObject);
                    break;

            }
        };

        $scope.$on('grid.header.invoke.row.action', function(event, data) {
            $scope.gridRowAction(data);
        });

        $scope.broadcastServiceMsg = BroadcastService.messageObject;

        $scope.$watch('broadcastServiceMsg', function() {
            var eventData = $scope.broadcastServiceMsg;

            if (eventData) {
                var correspondingGridEvent =
                    GridConfigurationService.convertTopLevelEventToGridEvent($scope.reportId, eventData.action);
                $scope.changedObject = {};
                if (correspondingGridEvent.length > 0) {
                    $scope.changedObject = {data: eventData, action: correspondingGridEvent};
                }

                BroadcastService.consumeMsg();
            }
        });

    }]);
