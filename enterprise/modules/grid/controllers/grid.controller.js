angular.module('bricata.ui.grid')
    .controller('GridController', ['$scope', 'GridConfigurationService',
        function($scope, GridConfigurationService) {
            $scope.isGridTopLevelMsg = false;
            $scope.isNoFilteredData = false;

            $scope.filterData = [];

            $scope.selectedRow = null;

            $scope.dataLoadErrorOccured = false;

            $scope.prepareQueryData = function() {
                var query = {};
                query.order_by = [$scope.sorting];

                if ($scope.filterData.length > 0) {
                    query.filters = $scope.filterData;
                }

                return JSON.stringify(query);
            };

            $scope.filterChanged = function(value) {
                var isReloadNeeded = value.length > 0 || $scope.filterData.length !== value.length;
                $scope.filterData = value;
                if (isReloadNeeded) {
                    $scope.pagination.currentPage = 1;
                    GridConfigurationService.savePageNumber(1);
                    $scope.reloadGridData();
                }
            };

            $scope.reloadDataAfterError = function() {
                $scope.resetGridOptions();
                $scope.sortTable($scope.gridConfiguration.defaultSorting.field);
                $scope.reloadGridData();
                $scope.dataLoadErrorOccured = false;
            };

            $scope.resetGridOptions = function() {
                GridConfigurationService.clearState();
                $scope.filterData = [];
                $scope.pageSize.selection = $scope.pageSize.options[0];
                $scope.pagination.currentPage = 1;
                $scope.$broadcast('clearFilterEvent');
            };

            $scope.handleNewDataLoaded = function(totalAvailable, totalLoaded, currentPageNumber, receivedData) {
                $scope.updateTopLevelTotal(totalAvailable);

                $scope.pagination.loadedItemsCount = totalLoaded;
                $scope.pagination.currentPage = currentPageNumber;
                GridConfigurationService.savePageNumber(currentPageNumber);

                $scope.checkFilterReturnedData();

                $scope.markChangedRow(receivedData);
            };

            $scope.updateTopLevelTotal = function(topLevelTotal) {
                $scope.pagination.totalItemsCount = topLevelTotal;
                $scope.$emit('policies.total.rows.change.event', $scope.pagination.totalItemsCount);
            };

            $scope.handleErrorData = function() {
                $scope.updateTopLevelTotal(0);
                $scope.dataLoadErrorOccured = true;
            };

            $scope.clearFilter = function() {
                $scope.$broadcast('clearFilterEvent');
                $scope.isNoFilteredData = false;
            };

            $scope.sortTable = function(columnField, sortDirection) {
                $scope.clearRowStatus();
                $scope.clearCheckBoxSelections();

                if (!sortDirection) {
                    sortDirection = $scope.tableParams.isSortBy(columnField, 'asc') ? 'desc' : 'asc';
                }
                $scope.tableParams.sorting(columnField,
                    sortDirection);

                $scope.sorting.field = columnField;
                $scope.sorting.direction = sortDirection;

                GridConfigurationService.saveSorting(columnField, sortDirection);
            };

            $scope.reloadGridData = function() {
                $scope.clearRowStatus();
                $scope.clearCheckBoxSelections();
                $scope.tableParams.reload();
            };

            $scope.pageChanged = function() {
                GridConfigurationService.savePageNumber($scope.pagination.currentPage);
                $scope.reloadGridData();
            };

            $scope.changePageSize = function() {
                $scope.pagination.currentPage = 1;
                GridConfigurationService.savePageNumber(1);
                GridConfigurationService.savePageSize($scope.pageSize.selection);
                $scope.reloadGridData();
            };

            $scope.rowAction = function(action, type, entityObjects) {
                var actionObject = {
                    actionName: action,
                    actionType: type,
                    data: entityObjects
                };

                $scope.rowActionHandler()(actionObject);
            };

            $scope.handleRowClick = function($event, entityObject, columnClick) {
                if (columnClick === 'check') {
                    return;
                }

                if (columnClick === 'action') {
                    $scope.selectedRow = null;
                    angular.forEach($scope.tableParams.data, function(item) {
                        item.$selected = false;
                    });
                    return;
                }

                $scope.clearRowStatus();

                angular.forEach($scope.tableParams.data, function(item) {
                    if (entityObject.id === item.id) {
                        if (item.$selected) {
                            item.$selected = false;
                            return;
                        }
                        item.$selected = true;
                        $scope.selectedRow = entityObject;
                    } else {
                        item.$selected = false;
                    }
                });
            };

            $scope.clearRowStatus = function() {
                if ($scope.tableParams && $scope.tableParams.data) {
                    angular.forEach($scope.tableParams.data, function(item) {
                        item.isChanged = false;
                    });
                }

                $scope.isGridTopLevelMsg = false;
            };

            $scope.handleDeleteStatus = function() {
                $scope.reloadGridData();
            };

            $scope.handleEditStatus = function() {
                $scope.reloadGridData();
            };

            $scope.handleCreateStatus = function() {
                $scope.resetGridOptions();
                $scope.sortTable($scope.gridConfiguration.createdSorting.field,
                    $scope.gridConfiguration.createdSorting.direction);
            };

            $scope.checkFilterReturnedData = function() {
                $scope.isNoFilteredData = $scope.pagination.totalItemsCount === 0 && $scope.filterData.length > 0;
            };

            $scope.bulkDeleteHandler = function() {
                var entityObjects =[];
                angular.forEach($scope.tableParams.data, function(item) {
                    if ($scope.checkboxes.items[item.id]) {
                        entityObjects.push(item);
                    }
                });
                $scope.rowAction('bulkDelete', 'modal', entityObjects);
            };

            $scope.clearCheckBoxSelections = function() {
                $scope.checkboxes = { 'checked': false, items: {} };
                $scope.selectAllProp = false;
                $scope.bulkDeleteDisabled = true;
            };

            $scope.markChangedRow = function(rawData) {
                if ($scope.changedObject && $scope.changedObject.data) {

                    $scope.rowChangedObject = angular.copy($scope.changedObject);

                    if ($scope.changedObject.data.id) {
                        $scope.isGridTopLevelMsg = false;

                        var isItemMarked = false;
                        var items = rawData ? rawData : $scope.tableParams.data;
                        angular.forEach(items, function(item) {
                            if (item.id === $scope.changedObject.data.id) {
                                item.isChanged = true;
                                isItemMarked = true;

                                if ($scope.changedObject.data.field && $scope.changedObject.data.value) {
                                    item[$scope.changedObject.data.field] = $scope.changedObject.data.value;
                                }
                            } else {
                                item.isChanged = false;
                            }
                        });

                        if (!isItemMarked) {
                            $scope.isGridTopLevelMsg = true;
                        }
                    } else {
                        $scope.isGridTopLevelMsg = true;
                    }

                    $scope.changedObject = null;
                }
            };

        }]);
