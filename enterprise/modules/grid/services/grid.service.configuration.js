angular.module("bricata.ui.grid")
    .factory("GridConfigurationService", ['PolicyGridConfigurationService', function(PolicyGridConfigurationService){

        var gridState = {
            pageNumber: -1,
            itemsPerPage: -1,
            selectedFilters: {},
            sorting: null
        };

        var service = {
            getConfiguration:function(reportId){

                var config = {};
                switch (reportId) {
                    case 'policies':
                        config = PolicyGridConfigurationService.getGridConfiguration();
                        break;

                }

                return config;
            },
            getModal:function(reportId, actionName) {
                var modalConfig = {};
                switch (reportId) {
                    case 'policies':
                        modalConfig = PolicyGridConfigurationService.getActionModal(actionName);
                        break;

                }

                return modalConfig;
            },
            redirectToPage:function(reportId, actionName) {
                switch (reportId) {
                    case 'policies':
                        PolicyGridConfigurationService.performRedirect(actionName);
                        break;

                }
            },
            convertTopLevelEventToGridEvent:function(reportId, actionName) {
                var gridEvent = '';
                switch (reportId) {
                    case 'policies':
                        if (actionName === 'policy-created') {
                            gridEvent = 'create';
                        } else if (actionName === 'policy-edited') {
                            gridEvent = 'edit';
                        }
                        break;
                }

                return gridEvent;
            },
            saveFilterState: function(key, value){
                gridState.selectedFilters[key] = value;
            },
            getFilterState: function(key){
                return gridState.selectedFilters[key];
            },
            clearFilterState: function(){
                gridState.selectedFilters = {};
            },
            isFilterStateEmpty: function(){
                var result = true;
                angular.forEach(gridState.selectedFilters, function (value) {
                    if (value) {
                        result = false;
                        return;
                    }
                });

                return result;
            },
            savePageNumber: function(pageNum){
                gridState.pageNumber = pageNum;
            },
            getPageNumber: function(){
                return gridState.pageNumber;
            },
            savePageSize: function(pageSize){
                gridState.itemsPerPage = pageSize;
            },
            getPageSize: function(){
                return gridState.itemsPerPage;
            },
            saveSorting: function(sortField, sortDirection) {
                gridState.sorting = {
                    field: sortField,
                    direction: sortDirection
                };
            },
            getSorting: function(){
                return gridState.sorting;
            },
            clearState: function(){
                gridState = {
                    pageNumber: -1,
                    itemsPerPage: -1,
                    selectedFilters: {},
                    sorting: null
                };
            }
        };
        return service;
    }]);