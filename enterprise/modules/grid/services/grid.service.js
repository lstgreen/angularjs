angular.module("bricata.ui.grid")
    .factory("GridCommonService", ["CommonGridItem", "moment", "CommonErrorMessageService",
        function(CommonGridItem, moment, CommonErrorMessageService){

        var fullData = {};
        var allGridItems = [];

        var formatData = function(columnDefinitions, dateFormat) {
            angular.forEach(allGridItems, function(item){
                angular.forEach(columnDefinitions, function(column){
                    switch(column.type) {
                        case 'date':
                            item[column.field] = moment(item[column.field]).format(dateFormat);
                            break;
                        case 'value':
                        case 'dynamic':
                            var valueToSet = '--empty--';
                            angular.forEach(column.values, function(columnValue){
                                if (columnValue.match === item[column.field]) {
                                    valueToSet = columnValue.value;
                                }
                            });
                            item[column.field] = valueToSet;
                            break;
                    }
                });
            });
        };

        var service = {
            getData:function($defer, params, filter, requestParams, dateFormat, columnDefinitions,
                             successHandler, errorHandler, noRestoreStatePending){

                if (noRestoreStatePending) {
                    CommonGridItem.query(requestParams).$promise.then(function (receivedData){
                        fullData = receivedData;
                        allGridItems = fullData.objects;
                        formatData(columnDefinitions, dateFormat);

                        params.total(allGridItems.length);

                        $defer.resolve(allGridItems);

                        successHandler(fullData.num_results, allGridItems.length, fullData.page, allGridItems);
                    }, function(reason) {
                        CommonErrorMessageService.showErrorMessage("errors.policyGridDataError", reason, null,
                            errorHandler);
                    });
                }
            }
        };
        return service;
    }]);