angular.module('bricata.ui.grid')
.factory('CommonGridItem', ['$resource', 'BricataUris',
        function($resource, BricataUris){
            return $resource(BricataUris.gridRequest, {entityId:'@id', query:'@q'}, {
                query: {method:'GET', isArray:false}
            });
}]);