angular.module('bricata.ui.grid')
    .factory('FilterValueItem', ['$resource', 'BricataUris',
        function($resource, BricataUris){
            return $resource(BricataUris.filterValueRequest, {entityId:'@id'}, {
                query: {method:'GET', isArray:false}
            });
        }]);
