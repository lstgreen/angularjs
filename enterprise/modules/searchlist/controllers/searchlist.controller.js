angular.module('bricata.ui.searchlist')
    .controller('searchListController', ['$scope',
    function($scope) {

        $scope.listModel = {};

        // Clearing Not results found text and returning to initial Signatures data list
        $scope.clearSearchValue = function() {
          $scope.searchedItem = '';
        };

        $scope.enableCheckBoxes = function() {
            $scope.selectAllProp = undefined;
            $scope.checkboxes = { 'checked': false, items: {} };

            $scope.selectAll = function($event) {
                var value = $event.target.checked;
                $scope.checkboxes.checked = value;
                $scope.changeAllCheckboxes(value);
            };

            $scope.changeAllCheckboxes = function(value) {
                angular.forEach($scope.listModel.entities, function(item) {
                    if (angular.isDefined(item.id)) {
                        $scope.checkboxes.items[item.id] = value;
                    }
                });
                $scope.processCheckBoxesChange();
            };

            //this part is needed only in case if deselecting categories should deselect signatures as well
            $scope.$watch('listModel', function() {
                $scope.updateTopCheckbox();
            }, true);
        };

        $scope.selectionUpdateHandler = function(event, eventData) {
            var selectionFlag = eventData.action === 'select';
            var changedEntityIds = eventData.ids;

            if (changedEntityIds.length === 0) {
                $scope.checkboxes.items = {};
                $scope.checkboxes.checked = false;
                $scope.selectAllProp = false;
                if (selectionFlag) {
                    $scope.changeAllCheckboxes(true);
                }
            } else {
                var ids = (changedEntityIds+'').split(',');
                angular.forEach(ids, function (id) {
                    $scope.checkboxes.items[id] = selectionFlag;
                });

                $scope.processCheckBoxesChange();
            }
        };

        $scope.processCheckBoxesChange = function() {
            if (!$scope.listModel.entities || $scope.listModel.entities.length === 0) {
                return;
            }

            $scope.updateTopCheckbox();

            $scope.selectionChanged()($scope.checkboxes.items);
        };

        $scope.updateTopCheckbox = function() {
            if (!angular.isDefined($scope.listModel.entities)) {
                return;
            }

            var checked = 0, unchecked = 0,
                total = $scope.listModel.entities.length;

            angular.forEach($scope.listModel.entities, function (item) {
                checked += ($scope.checkboxes.items[item.id]) || 0;
                unchecked += (!$scope.checkboxes.items[item.id]) || 0;
            });

            if ((unchecked === 0) || (checked === 0)) {
                $scope.checkboxes.checked = (checked === total && $scope.listModel.entities.length > 0);
            }

            $scope.selectAllProp = checked !== 0 && unchecked !== 0;
        };

    }]);
