angular.module("bricata.ui.searchlist")
    .directive("searchList", [
    function () {
        return {
            restrict: 'E',
            controller: 'searchListController',
            scope: {
                sectionTitle: '@',
                signatureIcon: '@',
                searchPlaceholder: '@',
                data: '=',
                selectionChanged: '&',
                clearAllSelected: '=',
                removeSelection: '=',
                absentDataMsg: '@',
                updateSelectionEventName: '@',
                updateTopSelectionEventName: '@',
                showRemoveButton: '=',
                preselectAll: '='
            },
            templateUrl: 'modules/searchlist/views/searchList.html',
            link: function(scope) {
                var preselectedIdsData = null;

                scope.$watch('data.length', function() {
                    scope.listModel.entities = scope.data;
                    scope.$emit('content.changed');

                    if (scope.preselectAll) {
                        scope.changeAllCheckboxes(true);
                    }

                    if (scope.listModel.entities && scope.listModel.entities.length > 0 && preselectedIdsData) {
                        scope.selectionUpdateHandler(null, preselectedIdsData);
                        preselectedIdsData = null;
                    }
                });

                scope.$watch('searchedItem', function(){
                    scope.$emit('content.changed');
                });

                if (angular.isDefined(scope.signatureIcon) && scope.signatureIcon === 'checkbox') {
                    scope.enableCheckBoxes();
                }

                if (angular.isDefined(scope.updateSelectionEventName)) {
                    scope.$on("update.available.signatures.selection.event", function(event, data) {
                        if (angular.isDefined(scope.listModel.entities)) {
                            scope.selectionUpdateHandler(event, data);
                        } else {
                            preselectedIdsData = data;
                        }

                    });
                }

                if (angular.isDefined(scope.updateTopSelectionEventName)) {
                    scope.$on("update.available.categories.selection.event", function(event, data) {
                        if (angular.isDefined(scope.listModel.entities)) {
                            scope.selectionUpdateHandler(event, data);
                        } else {
                            preselectedIdsData = data;
                        }
                    });
                }
            }
        };
    }]);