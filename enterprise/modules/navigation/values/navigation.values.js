angular.module("bricata.ui.navigation")
    .value("topMenuLinks", {
        getLinks: function(URIs) {
            return [
                {href: URIs.mainNavLinks.dashboardUrl, name: 'mainNavLinks.dashboard', type: 'external',
                    isAdmin :false},
                {href: URIs.mainNavLinks.adminUrl, name: 'mainNavLinks.administration', type: 'external',
                    isAdmin: true},
                {href: URIs.mainNavLinks.sensorsUrl, name: 'mainNavLinks.sensors', type: 'external',
                    isAdmin :false},
                {href: URIs.mainNavLinks.signaturesUrl, name: 'mainNavLinks.signatures', type: 'external',
                    isAdmin :false},
                {href: URIs.mainNavLinks.policiesUrl, name: 'mainNavLinks.policies', type: 'internal',
                    isAdmin :false},
                {href: URIs.mainNavLinks.eventsUrl, name: 'mainNavLinks.events', type: 'external',
                    isAdmin :false},
                {href: URIs.mainNavLinks.searchUrl, name: 'mainNavLinks.search', type: 'external',
                    isAdmin :false}
            ];
        }
    })
    .value("adminLinks", {
        getLinks: function(URIs) {
            return [
                {href: URIs.adminButtonDropDownLinks.settingsUrl, name: 'adminButtonLinks.generalSettings',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.classificationsUrl, name: 'adminButtonLinks.classifications',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.sensorsUrl, name: 'adminButtonLinks.sensors',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.lookupUrl, name: 'adminButtonLinks.lookupSources',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.nameManagerUrl, name: 'adminButtonLinks.assetNameManager',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.severitiesUrl, name: 'adminButtonLinks.severities',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.signaturesUrl, name: 'adminButtonLinks.signatures',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.usersUrl, name: 'adminButtonLinks.users',
                    type: 'external'},
                {href: URIs.adminButtonDropDownLinks.jobQueueUrl, name: 'adminButtonLinks.workerJobQueue',
                    type: 'external'}
            ];
        }
    });