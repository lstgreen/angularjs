angular.module('bricata.ui.navigation')
    .service('CommonNavigationService', ['$window', '$location', 'BricataUris',
        function($window, $location, BricataUris){

            this.navigateTo = function (url, linkType, searchObject) {
                switch (linkType) {
                    case 'external':
                        $window.location.href = url;
                        break;
                    case 'internal':
                        if (searchObject) {
                            $location.path(url).search(searchObject);
                        } else {
                            $location.path(url);
                        }
                        break;
                }
            };

            this.isThisCurrentLocation = function(currentLocation) {
                return $location.absUrl().indexOf(currentLocation) > 0;
            };

            this.navigateToPoliciesGridPage = function() {
                this.navigateTo(BricataUris.policiesPage, 'internal');
            };

            this.navigateToLoginPage = function() {
                this.navigateTo(BricataUris.loginPageLink, 'external');
            };

        }]);
