angular.module('bricata.ui.navigation')
    .controller('NavigationController',
    ['$scope', 'BricataUris', 'CommonNavigationService', 'topMenuLinks', 'adminLinks',
    function($scope, BricataUris, CommonNavigationService, topMenuLinks, adminLinks) {

        // Initializing the main top navigation links object
        $scope.mainNavLinks = topMenuLinks.getLinks(BricataUris);

        // Initializing the Administration button drop-down menu links
        $scope.adminButtonDropDown = adminLinks.getLinks(BricataUris);

        $scope.navigationClicked = function(url, linkType) {
            CommonNavigationService.navigateTo(url, linkType);
        };

        // Set active link in the view: ng-class="{active: isActiveLink(link.href)}"
        // Return true if the function param matches the current page location
        $scope.isActiveLink = function (linkUrl) {
            //this is temporary just for the first release, please uncomment last line to work correctly
            return linkUrl && linkUrl.indexOf(BricataUris.mainNavLinks.policiesUrl) === 0;

            //return CommonNavigationService.isThisCurrentLocation(linkUrl);
        };

    }]);
