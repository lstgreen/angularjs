angular.module('bricata.ui.errormsg')
    .controller('ErrorMessageModalController', ['$scope', '$modalInstance', 'CommonModalService', 'messageObject',
        function($scope, $modalInstance, CommonModalService, messageObject) {

            $scope.messageObject = messageObject;

            $scope.closeErrorMessageModal = function () {
                $modalInstance.close();
                CommonModalService.unbindRepositionOnResize();
            };

            $modalInstance.opened.then(function() {
                CommonModalService.centerModal();
                CommonModalService.bindRepositionOnResize();
            });

        }]);
