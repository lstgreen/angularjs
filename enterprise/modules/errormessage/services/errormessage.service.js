angular.module('bricata.ui.errormsg')
.service('CommonErrorMessageService', [ 'CommonModalService',
    function(CommonModalService){
        this.showErrorMessage = function(errorTxt, msgObj, errorTitle, callback) {
            var modalConfiguration = {
                templateUrl: 'modules/errormessage/views/error-message-modal.html',
                controller: 'ErrorMessageModalController',
                size: (msgObj && msgObj.data && msgObj.data.message) ? '' : 'sm',
                resolve: {
                    messageObject: function(){
                        return {
                            text: errorTxt,
                            msg: (msgObj && msgObj.data && msgObj.data.message) ? msgObj.data.message : '',
                            title: errorTitle ? errorTitle : 'errors.commonTitle'
                        };
                    }
                }
            };

            if (callback) {
                CommonModalService.show(modalConfiguration).then(function () {
                    callback();
                });
            } else {
                CommonModalService.show(modalConfiguration);
            }
        };

    }]);
