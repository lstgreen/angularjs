angular.module("bricata.ui.fileselect")
    .directive("fileSelect", [
    function () {
        return {
            restrict: 'E',
            templateUrl: 'modules/fileselect/views/file-select.html',
            link: function($scope, $element) {
                var button, fileField, proxy;
                fileField = angular.element($element[0].querySelector('[type="file"]')).on('change', function() {
                    proxy.val(angular.element(this).val()
                        .replace(/C:\\fakepath\\/i, '').replace(/\\/g, '/').replace(/.*\//, ''));
                });
                proxy = angular.element($element[0].querySelector('[type="text"]')).on('click', function() {
                    fileField.triggerHandler('click');
                });
                button = angular.element($element[0].querySelector('[type="button"]')).on('click', function() {
                    fileField.triggerHandler('click');
                });
            }
        };
    }]);