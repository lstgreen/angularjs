angular.module("bricata.ui.labletrunc")
    .directive("labelTrunc", ["$timeout", function($timeout) {
        return {
            restrict: "A",

            link: function(scope, element, attr) {

                scope.updateTooltip = function() {
                    attr.$set('tooltip', '');
                    var fullTextElem = element
                        .clone()
                        .css({display: 'inline', width: 'auto', visibility: 'hidden'});
                    element.parent().append(fullTextElem);

                    if (fullTextElem[0].offsetWidth > element[0].offsetWidth) {
                        attr.$set('tooltip', element.html());
                    }

                    fullTextElem.remove();
                };

                scope.$watch(function() { return element.html(); }, function() {
                    scope.updateTooltip();
                });

                //wait for angular bind
                $timeout(function(){
                    scope.updateTooltip();
                }, 333, false);
            }
        };
    }]);
