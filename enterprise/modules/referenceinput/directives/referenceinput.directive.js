angular.module("bricata.ui.referenceinput")
    .directive("referenceInput", [function () {
        return {
            restrict: 'E',
            templateUrl: 'modules/referenceinput/views/referenceinput.html',
            controller: 'ReferenceInputController'
        };
    }]);