angular.module("bricata.ui.ipinput")
    .directive("ipInput", [
        function() {
            return {
                restrict: 'E',
                templateUrl: 'modules/ipinput/views/ipinput.html',
                controller: 'ipInputController',
                scope: {
                    selectedIpAddress: "=",
                    selectedPort: "=",
                    topValidationResultName: "@",
                    validationGroup: "@"
                }
        };
    }]);