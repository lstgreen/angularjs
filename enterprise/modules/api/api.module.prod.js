angular.module('bricata.ui.api', [])
    .factory('BricataUris', [function(){
        return {
            configurationUrl: 'config/app_conf.json',

            userInfo: '../api/v1/current_user',
            gridRequest: '../api/v1/:entityId?page=:page&results_per_page=:pagesize&q=:query',
            filterValueRequest: '../api/v1/:entityId',

            policyItems: 'policies',
            policyItem: '../api/v1/policies:id',
            policyNames: '../api/v1/policies/lite',
            policyGridFilterValues: 'users',
            policyWizardPage: '/wizard',
            policiesPage: '/policies',

            signatureItems: '../api/v1/signature_categories',
            signaturesRequest: 'json-mocks/:id',
            signatureClassTypes: '../api/v1/signature_class_types',
            signatureReferenceTypes: '../api/v1/reference_types',
            sidIDNumber: '../api/v1/signatures/new_sid',
            sendSignatureForPreview: '../api/v1/signatures/preview',
            createNewSignature: '../api/v1/signatures',

            sensorItems: '../api/v1/sensors',
            policyDetailRequest: '../api/v1/policies/:rowId/:entityId',
            policyDetailSensors: 'sensors',
            policyDetailSignatures: 'signatures',

            // Main Navigation Links
            loginPageLink: '../users/login',
            logoutPageLink: '../users/logout',
            settingsPageLink: '../users/edit',
            logoLink: '../',

            // The 'name' values are set in the bundle_en.json file
            mainNavLinks: {
                dashboardUrl: '../dashboard',
                adminUrl: 'administration',
                sensorsUrl: '../sensors',
                signaturesUrl: '../signatures',
                policiesUrl: '/policies',
                eventsUrl: '../events/sessions',
                searchUrl: '../search'
            },

            // Administration button drop-down menu links
            // The 'name' values are set in the bundle_en.json file
            adminButtonDropDownLinks: {
                settingsUrl: '../settings',
                classificationsUrl: '../classifications',
                sensorsUrl: '../sensors',
                lookupUrl: '../lookups',
                nameManagerUrl: '../asset_names',
                severitiesUrl: '../severities',
                signaturesUrl: '../signatures',
                usersUrl: '../users',
                jobQueueUrl: '../jobs'
            }

        };
    }]);