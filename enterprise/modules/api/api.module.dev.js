angular.module('bricata.ui.api', [])
    .factory('BricataUris', [function(){
        return {
            configurationUrl: 'config/app_conf.json',

            userInfo: 'json-mocks/userinfo.json',
            gridRequest: 'json-mocks/:entityId?page=:page&results_per_page=:pagesize&q=:query',
            filterValueRequest: 'json-mocks/:entityId',

            policyItems: 'policies.json',
            policyItem: 'json-mocks/policies.json:id',
            policyNames: 'json-mocks/policies.json',
            policyGridFilterValues: 'authors.json',
            policyWizardPage: '/wizard',
            policiesPage: '/policies',

            signatureItems: 'json-mocks/signatures.categories.json',
            signaturesRequest: 'json-mocks/:id',
            signatureClassTypes: 'json-mocks/signature.class.types.json',
            signatureReferenceTypes: 'json-mocks/signature.reference.types.json',
            sidIDNumber: 'json-mocks/new_sid.json',
            sendSignatureForPreview: 'json-mocks/preview.json',
            createNewSignature: 'json-mocks/signature.new.json',

            sensorItems: 'json-mocks/sensors.json',
            policyDetailRequest: 'json-mocks/:entityId?q=:rowId',
            policyDetailSensors: 'policy.detail.sensors.json',
            policyDetailSignatures: 'policy.detail.signatures.json',

            // Main Navigation Links
            loginPageLink: '../users/login',
            logoutPageLink: '../users/logout',
            settingsPageLink: '../users/edit',
            logoLink: '../',

            // The 'name' values are set in the bundle_en.json file
            mainNavLinks: {
                dashboardUrl: 'http://google.com',
                adminUrl: 'administration',
                sensorsUrl: 'sensors',
                signaturesUrl: 'signatures',
                policiesUrl: '/policies',
                eventsUrl: 'events',
                searchUrl: 'search'
            },

            // Administration button drop-down menu links
            // The 'name' values are set in the bundle_en.json file
            adminButtonDropDownLinks: {
                settingsUrl: 'http://google.com',
                classificationsUrl: 'classifications',
                sensorsUrl: 'sensors',
                lookupUrl: 'lookup-sources',
                nameManagerUrl: 'asset-name-manager',
                severitiesUrl: 'severities',
                signaturesUrl: 'signatures',
                usersUrl: 'users',
                jobQueueUrl: 'worker-job-queue'
            }

        };
    }]);