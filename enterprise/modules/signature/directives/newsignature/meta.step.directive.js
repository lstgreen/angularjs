angular.module("bricata.ui.signature")
    .directive('metaInformation', function() {
        return {
            restrict: 'E',
            templateUrl: 'modules/signature/views/newsignature/meta-information.html'
        };
    });