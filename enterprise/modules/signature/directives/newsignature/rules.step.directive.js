angular.module("bricata.ui.signature")
    .directive('rulesInformation', function() {
        return {
            restrict: 'E',
            templateUrl: 'modules/signature/views/newsignature/rules-information.html'
        };
    });