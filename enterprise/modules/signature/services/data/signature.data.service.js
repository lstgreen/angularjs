angular.module('bricata.ui.signature')
    .factory("SignatureDataService",
    ["$q", "SignatureCreatePost", "SignatureSendPreviewPost", "SignatureGetSIDId",
        "SignatureCategoryItem", "SignatureClassTypeItem", "SignatureReferenceTypeItem",
        function ($q, SignatureCreatePost, SignatureSendPreviewPost, SignatureGetSIDId,
                  SignatureCategoryItem, SignatureClassTypeItem, SignatureReferenceTypeItem) {
            return {

                createSignature: function (data) {

                    var deferred = $q.defer();

                    SignatureCreatePost.save(data).$promise.then(function (sentData) {
                        deferred.resolve(sentData);
                    }, function (error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },

                sendDataForPreview: function (data) {

                    var deferred = $q.defer();

                    SignatureSendPreviewPost.save(data).$promise.then(function (sentData) {
                        deferred.resolve(sentData);
                    }, function (error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },

                getSignatureSID: function (data) {

                    var deferred = $q.defer();

                    SignatureGetSIDId.get(data).$promise.then(function (receivedData) {
                        deferred.resolve(receivedData);
                    }, function (error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },

                getSignatureCategories:function(){

                    var deferred = $q.defer();
                    SignatureCategoryItem.query().$promise.then(function (receivedData){
                        receivedData.objects.sort(function (a, b) {
                            if (a.name > b.name) {
                                return 1;
                            }
                            if (a.name < b.name) {
                                return -1;
                            }
                            return 0;
                        });
                        deferred.resolve(receivedData.objects);
                    }, function(error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },

                getClassTypes:function(){

                    var deferred = $q.defer();
                    SignatureClassTypeItem.query({results_per_page: 1000}).$promise.then(function (receivedData){
                        receivedData.objects.sort(function (a, b) {
                            if (a.name > b.name) {
                                return 1;
                            }
                            if (a.name < b.name) {
                                return -1;
                            }
                            return 0;
                        });

                        deferred.resolve(receivedData.objects);
                    }, function(error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },

                getReferenceTypes:function(){

                    var deferred = $q.defer();
                    SignatureReferenceTypeItem.query({results_per_page: 1000}).$promise.then(function (receivedData){
                        receivedData.objects.sort(function (a, b) {
                            if (a.name > b.name) {
                                return 1;
                            }
                            if (a.name < b.name) {
                                return -1;
                            }
                            return 0;
                        });

                        deferred.resolve(receivedData.objects);
                    }, function(error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            };

        }]);