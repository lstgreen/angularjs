angular.module('bricata.ui.policy')
    .factory('SignatureSendPreviewGet', ['$resource', 'BricataUris',
        function($resource, BricataUris){
            return $resource(BricataUris.sendSignatureForPreview,
                {ids:'@ids'}, {
                    query: {method:'GET', isArray:true}
                });
        }]);
