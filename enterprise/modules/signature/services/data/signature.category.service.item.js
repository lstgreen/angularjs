angular.module('bricata.ui.signature')
    .factory('SignatureCategoryItem', ['$resource', 'BricataUris',
        function($resource, BricataUris){
            return $resource(BricataUris.signatureItems, {entityId:'@id'}, {
                query: {method:'GET', isArray:false}
            });
        }]);