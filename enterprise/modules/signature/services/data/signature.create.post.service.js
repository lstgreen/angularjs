angular.module('bricata.ui.signature')
    .factory('SignatureCreatePost', ['$resource', 'BricataUris',
        function ($resource, BricataUris) {

            return $resource(BricataUris.createNewSignature, {}, {
                save: {method: 'POST'}
            });

        }]);