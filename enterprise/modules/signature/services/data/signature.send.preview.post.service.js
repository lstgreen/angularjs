angular.module('bricata.ui.signature')
    .factory('SignatureSendPreviewPost', ['$resource', 'BricataUris',
        function ($resource, BricataUris) {

            return $resource(BricataUris.sendSignatureForPreview, {}, {
                save: {method: 'POST'}
            });

        }]);