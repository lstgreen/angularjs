angular.module('bricata.ui.signature')
    .factory("SignatureWizardService", [
        'ConfigurationService', 'SignatureDataService', 'CommonModalService',
        function (ConfigurationService, SignatureDataService, CommonModalService) {
            return {
                initialtizeModel: function() {

                    var signatureModel = {};

                    signatureModel.data = {
                        action: null,
                        protocol: null,
                        source: {
                            ip: {ip: '', anyIp: false},
                            port: {port: '', anyPort: false}
                        },
                        unidirectional: true,
                        destination: {
                            ip: {ip: '', anyIp: false},
                            port: {port: '', anyPort: false}
                        },
                        name: '',
                        categoryId: null,
                        message: '',
                        flowControlTxt: '',
                        contentTxt: '',
                        classTypeID: null,
                        priority: null,
                        references: [{typeId: null, value: ''}],
                        sid: '',
                        gid: ''
                    };

                    signatureModel.values = {
                        actionList: ConfigurationService.getSignatureActions(),
                        protocolList: ConfigurationService.getSignatureProtocols(),
                        classTypeList: [],
                        referenceTypes: []
                    };

                    // The checkbox in the final step in the modal
                    signatureModel.previewSignaturesBeforeSavingCheckBox = {
                        checked : false
                    };

                    signatureModel.validation = {
                        first: {
                            isActionValid: false,
                            isProtocolValid: false,
                            isSourceValid: false,
                            isDestinationValid: false
                        },
                        second: {
                            isNameValid: false,
                            isCategoryValid: false
                        },
                        third: {
                            isReferenceValid: true,
                            noDuplicatedReferenceFound: true,
                            isSIDValid: false,
                            isGIDValid: false
                        }
                    };

                    return signatureModel;
                },

                getSignatureClassTypes: function(dataModel) {
                    SignatureDataService.getClassTypes().then(function(data) {
                        dataModel.classTypeList = data;
                    });
                },

                getSignatureReferenceTypes: function(dataModel) {
                    SignatureDataService.getReferenceTypes().then(function(data) {
                        dataModel.referenceTypes = data;
                    });
                },

                generateSIDAndGID: function(dataModel) {
                    SignatureDataService.getSignatureSID().then(function(data) {
                        dataModel.sid = data.sid;
                        dataModel.gid = 1;
                    });
                },

                previewSignature: function(signatureData, cancelActionMethod, submitActionMethod, previewLoadFail) {
                    if (signatureData.references.length === 1 && signatureData.references[0].typeId === null) {
                        signatureData.references = [];
                    }

                    SignatureDataService.sendDataForPreview(signatureData).then(function(previewData) {
                            CommonModalService.show({
                                templateUrl: 'modules/signature/views/preview-rule-modal.html',
                                backdrop: false,
                                controller: 'SignaturePreviewController',
                                resolve: {
                                    cancelCallback: function() {
                                        return function() {
                                            if (signatureData.references.length === 0) {
                                                signatureData.references = [{typeId: null, value: ''}];
                                            }
                                            cancelActionMethod();
                                        };
                                    },
                                    submitCallback: function() {
                                        return submitActionMethod;
                                    },
                                    ruleString: function() {
                                        return previewData;
                                    }
                                }
                            });

                        }, function(reason) {
                            if (signatureData.references.length === 0) {
                                signatureData.references = [{typeId: null, value: ''}];
                            }
                            previewLoadFail(reason);
                        });
                },

                saveSignature: function(data, successCallBack, errorCallback) {
                    if (data.references.length === 1 && data.references[0].typeId === null) {
                        data.references = [];
                    }
                    SignatureDataService.createSignature(data).then(function(response) {
                        successCallBack(response);
                    }, function(reason) {
                        if (data.references.length === 0) {
                            data.references = [{typeId: null, value: ''}];
                        }
                        errorCallback(reason);
                    });
                },

                processValidationResults: function(validationModel, eventData) {
                    if (angular.isDefined(eventData) && angular.isDefined(eventData.name) &&
                        eventData.name.indexOf('_') > 0) {
                        var validationProperty = eventData.name.split('_');

                        if (angular.isDefined(validationModel[validationProperty[0]]) &&
                            angular.isDefined(validationModel[validationProperty[0]][validationProperty[1]])) {
                            validationModel[validationProperty[0]][validationProperty[1]] = eventData.isValid;
                        }
                    }
                },

                isStepValid: function(validationStepModel) {
                    var isValid = true;
                    angular.forEach(validationStepModel, function(value) {
                        if (value === false) {
                            isValid = false;
                            return;
                        }
                    });

                    return isValid;
                }
            };

        }]);