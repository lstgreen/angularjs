angular.module("bricata.ui.signature")
    .value("ImportSignatureFormats", [
        {value: "suricata", i18Label: "importSignature.suricataFormat", checked: true},
        {value: "snort", i18Label: "importSignature.snortFormat", checked: false}
    ]);