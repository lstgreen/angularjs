angular.module('bricata.ui.signature')
    .controller('SignatureSelectController', ['$scope', '$rootScope', 'SignatureDataService', 'CommonModalService',
        function($scope, $rootScope, SignatureDataService, CommonModalService) {

            $scope.updateAvailableSignaturesSelectionEventName = "update.available.signatures.selection.event";
            $scope.updateAvailableCategoriesSelectionEventName = "update.available.categories.selection.event";

            $scope.isSignatureSelectionEnabled = false;

            $scope.isPreselectionNeeded = false;

            $scope.isLoadingSignatureCategories = true;
            $scope.loadSignatureCategories = function() {
                SignatureDataService.getSignatureCategories().then(function success(data) {
                    $scope.signatureCategories = data;
                    $scope.isLoadingSignatureCategories = false;
                });
            };

            $scope.loadSignatureCategories();

            $scope.syncUpCategory = function(syncUpCategoryId, newSignature) {
                var updatedCategory = {};
                angular.forEach($scope.signatureCategories, function (category) {
                    if (category.id === syncUpCategoryId) {
                        updatedCategory = category;
                        return;
                    }
                });

                updatedCategory.signatures.unshift(newSignature);

                var isCategorySelected = false;
                if ($scope.isSignatureSelectionEnabled) {
                    if ($scope.availableSignatures.length > 0) {
                        angular.forEach($scope.availableSignatures, function (availableSignature) {
                            if (availableSignature.category_id === syncUpCategoryId) {
                                isCategorySelected = true;
                                return;
                            }
                        });
                    }

                    if (isCategorySelected) {
                        $scope.availableSignatures.unshift(newSignature);
                    }

                } else {
                    if ($scope.selectionModel.length > 0) {
                        angular.forEach($scope.selectionModel, function (selectedSignature) {
                            if (selectedSignature.category_id === syncUpCategoryId) {
                                isCategorySelected = true;
                                return;
                            }
                        });

                        if (isCategorySelected) {
                            $scope.selectionModel.unshift(newSignature);
                        }
                    }
                }
            };

            $rootScope.$on('new.signature.added', function(event, data) {
                $scope.syncUpCategoryId = data.categoryId;
                $scope.syncUpCategory(data.categoryId, data.signature);
            });

            $scope.availableSignatures = [];
            $scope.categorySelectionChange = function(selectedCategoryIDs) {

                if ($scope.isSignatureSelectionEnabled) {
                    $scope.availableSignatures = [];
                } else {
                    $scope.selectionModel = [];
                }

                angular.forEach(selectedCategoryIDs, function(isSelected, categoryId) {
                    if (isSelected) {
                        var categoryIdNumber = parseInt(categoryId);
                        angular.forEach($scope.signatureCategories, function (category) {
                            if (category.id === categoryIdNumber) {
                                if ($scope.isSignatureSelectionEnabled) {
                                    $scope.availableSignatures = $scope.availableSignatures.concat(category.signatures);
                                } else {
                                    $scope.selectionModel = $scope.selectionModel.concat(category.signatures);
                                }

                                return;
                            }
                        });
                    }
                });
            };


            /* Selecting Available Signatures and passing them to Selected Signatures
             @selectedSignatureIDs - object with all checked signatures ids - {1: true, 2: true ...}
             */
            $scope.signaturesSelectionChange = function (selectedSignatureIDs) {

                /* $scope.selectedSignatures - The selected signatures in array.
                 Emptying on function run (checking box in the available signatures and refill below
                 */
                angular.forEach(selectedSignatureIDs, function (isSelected, signatureId) {
                    // @signatureId - the id of the selected signature from the available signatures list
                    var signatureIdNumber = parseInt(signatureId);
                    if (isSelected) {
                        angular.forEach($scope.availableSignatures, function (signature) {
                            if (signature.id === signatureIdNumber && $scope.selectionModel.indexOf(signature) === -1) {
                                $scope.selectionModel = $scope.selectionModel.concat(signature);
                                return;
                            }
                        });
                    } else {
                        angular.forEach($scope.selectionModel, function (selectedSignature, index) {
                            if (selectedSignature.id === signatureIdNumber) {
                                $scope.selectionModel.splice(index, 1);
                                return;
                            }
                        });
                    }
                });
            };

            $scope.selectAllCategories = function() {
                $scope.$broadcast($scope.updateAvailableCategoriesSelectionEventName, {ids: '', action: 'select'});
            };

            $scope.clearAll = function() {
                $scope.selectionModel = [];
                $scope.availableSignatures = [];
                $scope.$broadcast($scope.updateAvailableSignaturesSelectionEventName, {ids: '', action: 'deselect'});
                $scope.$broadcast($scope.updateAvailableCategoriesSelectionEventName, {ids: '', action: 'deselect'});
            };

            // Clearing all items from selected signatures
            $scope.clearAllSelected = function() {
                $scope.selectionModel = [];
                $scope.$broadcast($scope.updateAvailableSignaturesSelectionEventName, {ids: '', action: 'deselect'});

                if (!$scope.isSignatureSelectionEnabled) {
                    $scope.availableSignatures = [];
                    $scope.$broadcast($scope.updateAvailableCategoriesSelectionEventName,
                        {ids: '', action: 'deselect'});
                }
            };

            // Remove current clicked signature
            $scope.removeSelection = function(removedSignatureId) {
                var removedSignatureIdNumber = parseInt(removedSignatureId);

                angular.forEach($scope.selectionModel, function (signature, index) {
                    if (signature.id === removedSignatureIdNumber) {
                        $scope.selectionModel.splice(index, 1);
                        $scope.$broadcast($scope.updateAvailableSignaturesSelectionEventName,
                            {ids: removedSignatureId, action: 'deselect'});
                        return;
                    }
                });
            };

            $scope.changeSelectionMode = function() {
                if (!$scope.isPreselectionNeeded) {
                    $scope.clearAll();
                }

                switch ($scope.selectionMode) {
                    case 'categories':
                        $scope.isSignatureSelectionEnabled = false;

                        if ($scope.isPreselectionNeeded) {
                            var signatureCategories = [];
                            angular.forEach($scope.preselectionModel, function(selectedSignature) {
                                if (signatureCategories.indexOf(selectedSignature.category_id) === -1) {
                                    signatureCategories.push(selectedSignature.category_id);
                                }
                            });
                            $scope.$broadcast($scope.updateAvailableCategoriesSelectionEventName,
                                {ids: signatureCategories.join(','), action: 'select'});
                        }

                        break;
                    case 'signatures':
                        $scope.isSignatureSelectionEnabled = true;
                        $scope.selectAllCategories();

                        if ($scope.isPreselectionNeeded) {
                            var signatures = [];
                            angular.forEach($scope.preselectionModel, function(selectedSignature) {
                                signatures.push(selectedSignature.id);
                            });
                            $scope.$broadcast($scope.updateAvailableSignaturesSelectionEventName,
                                {ids: signatures.join(','), action: 'select'});
                        }
                        break;
                }

                $scope.isPreselectionNeeded = false;
                $scope.preselectionModel = [];
            };

            $scope.checkPreselection = function() {
                if ($scope.preselectionModel.length > 0) {
                    $scope.isPreselectionNeeded = true;
                }
            };

            $scope.checkPreselection();

            /* ########################################
             Import Signature - Importing rules files
             ######################################## */
            $scope.openImportSignatureModal = function() {
                CommonModalService.show({
                    templateUrl: 'modules/signature/views/import-signature-modal.html',
                    controller: 'ImportSignatureController',
                    resolve: {
                        categoriesData: function(){
                            return $scope.signatureCategories;
                        }
                    }
                });
            };

            /* ########################################
             Add New Signature Wizard
             ######################################## */
            $scope.addNewSignature = function() {

                CommonModalService.show({
                    templateUrl: 'modules/signature/views/add-new-signature-modal.html',
                    windowClass: 'new-signature-modal-window',
                    controller: 'NewSignatureModalController',
                    resolve: {
                        categoriesData: function(){
                            var categories = angular.copy($scope.signatureCategories);
                            angular.forEach(categories, function(category) {
                                delete category.signatures;
                            });
                            return categories;
                        }
                    }
                });

            };

        }]);
