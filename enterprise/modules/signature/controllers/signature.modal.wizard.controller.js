angular.module('bricata.ui.signature')
    .controller('NewSignatureModalController',
    ['$scope', '$modalInstance', '$rootScope', 'CommonModalService', 'categoriesData', 'SignatureWizardService',
        'ConfigurationService', 'CommonErrorMessageService',
        function($scope, $modalInstance, $rootScope, CommonModalService, categoriesData, SignatureWizardService,
                 ConfigurationService, CommonErrorMessageService) {

            $scope.helpLinks = ConfigurationService.getSignatureHelpLinks();

            $scope.isPreviewShown = false;

            $scope.closeSignatureModal = function () {
                $rootScope.$emit('enable.validation');
                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();

                $scope.$destroy();
            };

            $modalInstance.opened.then(function() {
                $rootScope.$emit('disable.validation');
                CommonModalService.centerModal();
                CommonModalService.bindRepositionOnResize();
            });

            $scope.addNewSignatureModel = SignatureWizardService.initialtizeModel();
            SignatureWizardService.generateSIDAndGID($scope.addNewSignatureModel.data);
            SignatureWizardService.getSignatureClassTypes($scope.addNewSignatureModel.values);
            SignatureWizardService.getSignatureReferenceTypes($scope.addNewSignatureModel.values);

            $scope.addNewSignatureModel.categories = categoriesData;

            $scope.allowedSteps = {
                first: true,
                second: false,
                third: false,
                final: false
            };

            $scope.currentStep = {
                first: true,
                second: false,
                third: false
            };

            var unbindIpValidationListener = $scope.$on('ip.input.validation.processed', function(event, data) {
                SignatureWizardService.processValidationResults($scope.addNewSignatureModel.validation, data);

                $scope.processValidationResult();
            });

            var unbindInputValidationListener = $scope.$on('input.text.validation.processed', function(event, data) {
                SignatureWizardService.processValidationResults($scope.addNewSignatureModel.validation, data);

                $scope.processValidationResult();
            });


            var unbindReferenceValidationListener =
                $scope.$on('reference.input.validation.processed', function(event, data) {
                    data.name = 'third_isReferenceValid';
                    SignatureWizardService.processValidationResults($scope.addNewSignatureModel.validation, data);

                    $scope.processValidationResult();
                });


            $scope.processValidationResult = function() {
                $scope.allowedSteps.second =
                    SignatureWizardService.isStepValid($scope.addNewSignatureModel.validation.first);

                $scope.allowedSteps.third = $scope.allowedSteps.second &&
                    SignatureWizardService.isStepValid($scope.addNewSignatureModel.validation.second);

                $scope.allowedSteps.final =
                    SignatureWizardService.isStepValid($scope.addNewSignatureModel.validation.third);
            };


            // Changing form switch states - going to next steps
            $scope.addNewSignatureModel.goToRulesInformation = function() {
                if ($scope.allowedSteps.first) {
                    $scope.currentStep.first = true;
                    $scope.currentStep.second = $scope.currentStep.third = false;
                }
            };
            $scope.addNewSignatureModel.goToSignatureInformation = function() {
                $scope.processValidationResult();

                if (!$scope.allowedSteps.second) {
                    $rootScope.$broadcast('run.validation', {group: 'signatureRulesValidation'});
                    return;
                }

                $scope.currentStep.second = true;
                $scope.currentStep.first = $scope.currentStep.third = false;
            };
            $scope.addNewSignatureModel.goToMetaInformation = function() {
                $scope.processValidationResult();

                if (!$scope.allowedSteps.third) {
                    if ($scope.currentStep.first) {
                        $rootScope.$broadcast('run.validation', {group: 'signatureRulesValidation'});
                    }

                    if ($scope.currentStep.second) {
                        $rootScope.$broadcast('run.validation', {group: 'signatureEntityValidation'});
                    }

                    return;
                }

                $scope.currentStep.third = true;
                $scope.currentStep.first = $scope.currentStep.second = false;
            };

            $scope.closePreviewRuleModal = function () {
                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();
                $scope.$destroy();
            };

            $scope.previewCancel = function() {
                $scope.isPreviewShown = false;
                CommonModalService.centerModal();
            };

            $scope.previewSubmit = function() {
                $scope.saveAndClose();
            };

            $scope.previewLoadFail = function(reason) {
                $scope.isPreviewShown = false;
                CommonModalService.centerModal();

                CommonErrorMessageService.showErrorMessage("errors.previewSignatureError", reason);
            };

            $scope.createSignature = function() {
                $scope.processValidationResult();

                if (!$scope.allowedSteps.final) {
                    $rootScope.$broadcast('run.validation', {group: 'signatureMetaValidation'});

                    return;
                }


                if ($scope.addNewSignatureModel.previewSignaturesBeforeSavingCheckBox.checked === true) {
                    $scope.isPreviewShown = true;

                    SignatureWizardService.previewSignature($scope.addNewSignatureModel.data, $scope.previewCancel,
                        $scope.previewSubmit, $scope.previewLoadFail);
                } else {
                    $scope.saveAndClose();
                }
            };

            $scope.saveAndClose = function() {
                SignatureWizardService.saveSignature($scope.addNewSignatureModel.data,
                    $scope.handleSuccess, $scope.handleError);
            };

            $scope.handleSuccess = function(newSignatureData) {
                $rootScope.$emit('new.signature.added',
                    {categoryId: newSignatureData.category_id, signature: newSignatureData});
                $modalInstance.close();
                CommonModalService.unbindRepositionOnResize();
            };

            $scope.handleError = function(reason) {
                CommonErrorMessageService.showErrorMessage("errors.createSignatureError", reason);
            };

            //cleaning resources
            var unbindDestroy = $scope.$on("$destroy", function() {
                unbindIpValidationListener();
                unbindInputValidationListener();
                unbindReferenceValidationListener();
                unbindDestroy();
            });

        }]);
