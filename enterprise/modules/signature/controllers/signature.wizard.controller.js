/*
angular.module('bricata.ui.signature')
    .controller('SignatureWizardController',
    ['$scope', 'CommonNavigationService', 'SignatureWizardService',
        'SignatureDataService', 'ConfigurationService',
        function($scope, CommonNavigationService, SignatureWizardService, SignatureDataService, ConfigurationService) {

            $scope.helpLinks = ConfigurationService.getSignatureHelpLinks();

            $scope.addNewSignatureModel = SignatureWizardService.initialtizeModel();
            SignatureWizardService.generateSIDAndGID($scope.addNewSignatureModel.data);

            $scope.addNewSignatureModel.sendSignatureData = function() {
                SignatureWizardService.saveSignature();
            };

            SignatureDataService.getSignatureCategories().then(function success(data) {
                $scope.addNewSignatureModel.categories = data;
            });

            function prepareAndSendData() {

                var filledSignatureData = {};

                filledSignatureData.selectedAction = $scope.addNewSignatureModel.data.action;
                filledSignatureData.selectedProtocol = $scope.addNewSignatureModel.data.protocol.name;
                filledSignatureData.selectedSource = $scope.addNewSignatureModel.data.source.ip;
                filledSignatureData.selectedDirection = $scope.addNewSignatureModel.directions;
                filledSignatureData.signatureName = $scope.addNewSignatureModel.data.name;
                filledSignatureData.signatureMessage = $scope.addNewSignatureModel.data.message;
                filledSignatureData.signatureFlowControl = $scope.addNewSignatureModel.data.flowControlTxt;
                filledSignatureData.signatureContent = $scope.addNewSignatureModel.data.contentTxt;
                filledSignatureData.signatureClassType = $scope.addNewSignatureModel.classTypeList.type;
                filledSignatureData.signatureReference = $scope.addNewSignatureModel.referenceTypes;
                filledSignatureData.signatureSID = $scope.addNewSignatureModel.SIDList.SID;
                filledSignatureData.signatureGID = $scope.addNewSignatureModel.GIDList.GID;


            }




            $scope.addNewSignatureModel.createSignature = function() {
                if ($scope.addNewSignatureModel.previewSignaturesBeforeSavingCheckBox.checked === true) {
                    // Changing the form switch state with imported rules from the file for previewing
                    $scope.addNewSignatureModel.informationType = 'previewSignaturesBeforeSaving';
                }  else {
                    // Send rules data to somewhere
                    SignatureWizardService.saveSignature();

                    prepareAndSendData();

                }
            };

            $scope.cancelWizard = function() {
                CommonNavigationService.navigateToPoliciesGridPage();
            };

        }]);
*/