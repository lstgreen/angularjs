angular.module('bricata.ui.signature')
    .controller('ImportSignatureController',
    ['$scope', '$modalInstance', 'CommonModalService', 'categoriesData', 'ImportSignatureFormats',
        function($scope, $modalInstance, CommonModalService, categoriesData, ImportSignatureFormats) {

            $scope.categories = categoriesData;
            $scope.selectedCategory = $scope.categories[0];

            $scope.formats = ImportSignatureFormats;

            $scope.importSignature = function() {

            };

            // Checkbox for Preview Signatures before Saving
            $scope.previewSignaturesBeforeSaveCheckBox = {
                checked : false
            };

            // Switch state in Import form with file upload input
            $scope.importSignatureState = 'importSignatureFileUploadForm';

            // Save imported signature. Executes on Save Signature button in the modal window form
            $scope.saveSignature = function() {

                // If the "Preview Signatures before saving" checkbox is checked
                if ($scope.previewSignaturesBeforeSaveCheckBox.checked === true) {

                    // Changing the form switch state with imported rules from the file for previewing
                    $scope.importSignatureState = 'previewSignaturesBeforeSave';

                }
                else {
                    // Send rules data to somewhere
                    console.log('Sending rules data...');
                }

                $modalInstance.close();
                CommonModalService.unbindRepositionOnResize();

            };

            $scope.closePreviewModal = function () {
                $modalInstance.dismiss('cancel');
                CommonModalService.unbindRepositionOnResize();
            };

            $modalInstance.opened.then(function() {
                CommonModalService.centerModal();
                CommonModalService.bindRepositionOnResize();
            });

        }]);
