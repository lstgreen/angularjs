window.BricataPoliciesPage = angular.module("BricataPoliciesPage",
    ["ngRoute", "jm.i18next", "bricata.ui.templates", "bricata.ui.grid", "ngScrollable", "bricata.ui.policy",
        "bricata.ui.signature", "bricata.ui.userinfo", "bricata.ui.navigation", "bricata.ui.referenceinput",
        "bricata.ui.topcheckbox", "bricata.ui.broadcast", "bricata.ui.preloader", "bricata.ui.errormsg",
        "bricata.ui.configuration"])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider
            .when('/policies',{
                templateUrl: 'modules/views/policies.html'
            })
            .when('/wizard',{
                templateUrl: 'modules/views/create-policy.html'
            })
            /*.when('/create-signature',{
                templateUrl: 'modules/views/add-new-signature.html'
            })*/
            .otherwise({ redirectTo: '/policies' });
    }]).config(['$tooltipProvider', function($tooltipProvider){
        $tooltipProvider.setTriggers({
            'mouseenter': 'mouseleave',
            'click': 'click',
            'focus': 'blur',
            'validationfailed': 'validationpassed'
        });
    }]).controller("MainController", ["$scope", "UserInfoService", "CommonNavigationService", "ConfigurationService",
        function($scope, UserInfoService, CommonNavigationService, ConfigurationService) {

            $scope.translationLoaded = false;
            $scope.userInfoLoaded = true;
            $scope.configurationLoaded = false;

            $scope.$on('i18nextLanguageChange', function () {
                $scope.translationLoaded = true;
            });

            UserInfoService.getUserInfo().then(function success(data) {
                $scope.userData = data;
                $scope.userInfoLoaded = true;
            }, function error() {
                //CommonNavigationService.navigateToLoginPage();
                $scope.userInfoLoaded = true;
            });

            ConfigurationService.loadConfiguration().then(function(data) {
                ConfigurationService.setConfiguration(data);
                $scope.configurationLoaded = true;
            });
    }]);

var LOCALIZATION_NAME_SPACE = "bundle";

angular.module('jm.i18next').config(['$i18nextProvider', function ($i18nextProvider) {
    $i18nextProvider.options = {
        ns: {
            namespaces: [LOCALIZATION_NAME_SPACE],
            defaultNs: LOCALIZATION_NAME_SPACE
        },
        resGetPath: "i18n/__ns_____lng__.json",
        fallbackLng: "en",
        lng: "en"
    };
}]);